.PHONY: install format lint test audit
install:
	@poetry install
uninstall:
	pip uninstall tipsarena_core
format:
	@blue .
	@isort .
lint:
	@blue --check .
	@isort --check .
	@prospector --with-tool pep257 --doc-warning
test:
	@pytest tipsarena_tests/ -v
test_cov:
	 pytest tests/  --cov=tipsarena_core

audit:
	@pip-audit

cleanup:
	@rm -r .pytest_cache/
	@rm -r build/
	@rm -r dist/
	@rm -r tips_arena_core.egg-info/
	@echo "Limpeza concluída..."