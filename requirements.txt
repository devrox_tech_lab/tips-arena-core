pymongo
beautifulsoup4
bs4
requests
selenium
urllib3
pytz
python-dotenv
PyJWT
bcrypt
pika
pydantic
click