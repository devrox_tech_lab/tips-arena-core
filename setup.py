# -*- coding: utf-8 -*-
""""
Tips Arena Setup
Instala o Tips Arena Core e Shell
"""
import os

from setuptools import find_packages, setup

README = os.path.join(os.path.dirname(__file__), 'README.md')
REQUIREMENTS = os.path.join(os.path.dirname(__file__), 'requirements.txt')
REQUIREMENTS_DEV = os.path.join(os.path.dirname(__file__), 'requirements-dev.txt')


def obterModulos(filename):
    return [req.strip() for req in open(filename).readlines()]


setup(
    name="tips-arena-core",
    version="0.1.0",
    description="Tips Arena Core",
    long_description=open(README).read(),
    author="DevRocks.Tech",
    author_email="devrocks.tech@gmail.com",
    packages=find_packages(),
    include_package_data=True,
    install_requires=obterModulos(REQUIREMENTS),
    extras_require={
        "dev": [obterModulos(REQUIREMENTS_DEV)]
    },
    entry_points={
        'console_scripts': ['tash = tipsarena_shell']
    }
)
