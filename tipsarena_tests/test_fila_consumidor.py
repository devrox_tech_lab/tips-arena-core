# -*- coding: utf-8 -*-

from tipsarena_core import gerenciador_filas


def teste_gerar_consumer():
    gerenciador_filas.consumirMensagens("ta-fila-testepy", gerenciador_filas.callbackDefault)


if __name__ == "__main__":
    teste_gerar_consumer()
