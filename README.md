Tips Arena Core
_______________

Fluxo extração e processamento de dados

1. Extrair as partidas do dia e as dos 2 próximos dias extrairHtmlPartidasDia(0,1,2)
    * Salva o html das partidas do dia no diretório "/para_processar/partidas/partidas_dia/"
    * O arquivo é salvo com o nome "ptd-dia-yyyymmdd-SS", ex.: "ptd-dia-20220725-SE.html"
    * É incluída na fila "FILA.FL_PROC_HTML_PARTIDAS_DIA" para processamento do HTML das partidas do dia

2. Processar HTML das partidas do dia processarHtmlListaDePartidas(caminhoParaArquivo: str)
    * Lê registros da fila "FILA.FL_PROC_HTML_PARTIDAS_DIA"
    * Lê o arquivo html das partidas do dia
    * Obtém a lista de partidas do dia do arquivo
    * Para cada partida da lista é incluído um registro na fila "FILA.FL_EXT_HTML_PARTIDA" para extração dos HTML`s das
      partidas
    * Move arquivo de partidas do dia para "/para_processar/partidas/partidas_dia/"

3. Extrair HTML de uma partida extrairHtmlPartida(dadosPartida: dict)
    * Lê registros da fila "FILA.FL_EXT_HTML_PARTIDA"
    * Salva o html da partida no diretório "/para_processar/partidas/partida/"
    * O arquivo é salvo com o nome "ptd-[hash da url da partida]"
    * Inclui na fila "FILA.FL_PROC_HTML_PARTIDA" para processamento do HTML da partida

4. Processar HTML de uma partida processarHtmlListaDePartidas(caminhoParaArquivo: str)
    * Lê registros da fila "FILA.FL_PROC_HTML_PARTIDA"
    * Abre o arquivo html da partida
    * Obtém os dados da partida
    * Salva os dados da partida no banco de dados
    * Caso a informação de classificação esteja disponível, inclui registro na fila "
      FILA.FL_EXT_HTML_CLASSIFICACAO_PARTIDA" para extracao da classificação da competição

5. Extrair HTML da classificação da competição de uma partida
    * Lê os registros da fila "FILA.FL_EXT_HTML_CLASSIFICACAO_PARTIDA"
    * Salva o html da classificação na pasta ""
    * O arquivo é salvo com o nome "ptd-cls-[hash da url da partida]"
    * Inclui registro na fila "" para processamento do HTML da classificação

6. Processar HTML da classificação da competição da partida
    * Lê os registros da fila "FL_EXT_HTML_CLASSIFICACAO_PARTIDA"
    * Abre o arquivo html da classificação
    * Obtém os dados da classificação da competicão
    * Salva dados da classificação no banco de dados

-----------------

1. Fazer a extração do HTML da lista de países
    * Insere o HTML na fila de processamento de países.
2. Processar o HTML da lista de países
    * Obtem as URLs de competições de cada país.
    * Insere a URL do país na fila de extração de competições do país.
3. Extrair o HTML das competições para cada URL de país.
    * Insere o HTML na fila de processamento de edições de competição
4. Processar o HTML das competições de cada país
    * Obtem as URLs das edições de cada competição
    * Insere a URL de cada edição da competição na fila de extração de edições da competição.
5. Extrair o HTML edições da competição
    * Insere HTML da edições da competição contendo a lista na fila de processamento de edições.
6. Processar o HTML de edições da competição
    * Obtem as URLs de edições de uma competição
    * Insere cada URL de edição de competição nas filas
        - extração do HTML das partidas da edição da competição.
        - extração do HTML da edição da competição.
        - extração do HTML das equipes da edição da competição
7. Extrair HTML partidas da edição da competição
    * Insere o HTML na fila de processamento de partidas da edição da competição.
8. Processar HTML partidas da edição da competição
    * Obtem as URLs das partidas da edição da competição
    * Insere cada URL de partida na fila de extração de partida.
9. Extrair HTML equipes edição da competição
    * Insere o HTML na fila de processamento de equipes da edição da competição. 10)Processar HTML equipes da edição da
      competição
    * Obtem as URLs das equipes da edição.
    * Insere cada URL de equipe na fila de extração de equipe

Licença
-------

Copyright (C) 2018 DevRox Tech (Daniel Cirino).
