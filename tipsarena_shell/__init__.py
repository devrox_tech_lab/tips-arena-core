# -*- coding: utf-8 -*-
import click

from tipsarena_core.services import log_service as log
from tipsarena_shell.extracao import cli_ext
from tipsarena_shell.processamento import cli_proc


@click.group('cli')
def cli():
    """"Interface de linha de comando para execução de rotinas e gerenciamento da aplicação Tips Arena."""
    try:
        pass
    except KeyboardInterrupt:
        log.INFO("Processo interrompido!")


cli.add_command(cli_ext)
cli.add_command(cli_proc)

cli()
