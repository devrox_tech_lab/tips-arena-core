import click

from tipsarena_core.gerenciador_filas.processamento import (
    processamento_partida, processamento_partida_classificacao,
    processamento_partidas_dia, processamento_paises, processamento_competicoes_pais, processamento_edicoes_competicao)


@click.group("proc")
def cli_proc():
    """" Utilitário para processamento de dados"""


@cli_proc.command("proc_html_paises")
def consumirFilaProcessamentoPaises():
    """"Inicia o consumidor da fila de processamento de html dos paises."""
    processamento_paises.iniciar()


@cli_proc.command("proc_html_cpt_pais")
def consumirFilaProcessamentoCompeticoesPais():
    """"Inicia o consumidor da fila de processamento de html das competicoes de um paises."""
    processamento_competicoes_pais.iniciar()


@cli_proc.command("proc_html_edcs_cpt")
def consumirFilaProcessamentoEdicoesCompeticao():
    """"Inicia o consumidor da fila de processamento de html das edicoes de uma competicao."""
    processamento_edicoes_competicao.iniciar()


@cli_proc.command("proc_html_ptd_dia")
def consumirFilaProcessamentoPartidasDoDia():
    """"Inicia o consumidor da fila de processamento de html das partidas do dia."""
    processamento_partidas_dia.iniciar()


@cli_proc.command("proc_html_ptd_dia")
def consumirFilaProcessamentoHtmlPartidasDoDia():
    """"
      Inicia o consumidor da fila de processamento de HTML de partidas do dia.
    """
    processamento_partidas_dia.iniciar()


@cli_proc.command("proc_html_ptd")
def consumirFilaProcessamentoHtmlPartida():
    """"
    Inicia o consumidor da fila de processamento de HTML da partida
    """
    processamento_partida.iniciar()


@cli_proc.command("proc_html_cls_ptd")
def consumirFilaProcessamentoHtmlClassificacaoPartida():
    """"
    Inicia o cosumidor da fila de processamento de HTML da partida
    """
    processamento_partida_classificacao.iniciar()
