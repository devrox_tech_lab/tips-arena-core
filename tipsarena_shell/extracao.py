import click

from tipsarena_core.extratores import motor_extracao_flashscore
from tipsarena_core.gerenciador_filas.extracao import (
  extracao_partida, extracao_partida_classificacao, extracao_competicoes_pais, extracao_edicoes_competicao)


@click.group("ext")
def cli_ext():
    """" Utilitário para extração de dados"""


@cli_ext.command('ext_html_paises')
def extrairHtmlPaises():
    """"Extrair a lista de países."""
    motor_extracao_flashscore.extrairHtmlPaises()


@cli_ext.command('ext_html_cpt_pais')
def extrairHtmlCompeticoesPais():
    """Extrair a lista de competições de um pais."""
    extracao_competicoes_pais.iniciar()


@cli_ext.command('ext_html_edc_cpt')
def extrairHtmlEdicoesCompeticao():
    extracao_edicoes_competicao.iniciar()


@cli_ext.command("ext_html_ptd_dia")
@click.option('-d', 'dia', type=click.INT, default=0)
def extrairHtmlPartidasDoDia(dia: int):
    """"
    Faz a extração de partidas do dia de acordo com o índice passado:
    -1-> Para o dia de ontem
    -2-> Para o dia de hoje menos 2 dias
    -3-> Para o dia de hoje menos 3 dias
    -4-> Para o dia de hoje menos 4 dias
    -5-> Para o dia de hoje menos 5 dias
    -6-> Para o dia de hoje menos 6 dias
    -7-> Para o dia de hoje menos 7 dias
    0-> Para o dia de hoje
    1-> Para o dia de amanhã
    2-> Para o dia de hoje mais 2 dias
    3-> Para o dia de hoje mais 3 dias
    4-> Para o dia de hoje mais 4 dias
    5-> Para o dia de hoje mais 5 dias
    6-> Para o dia de hoje mais 6 dias
    7-> Para o dia de hoje mais 7 dias
    """
    motor_extracao_flashscore.extrairPartidasDia(dia)


@cli_ext.command("ext_html_ptd")
def consumirFilaExtracaoHtmlPartidas():
    """"
    Inicia o consumidor da fila para extração do HTML de uma partida
    """
    extracao_partida.iniciar()


@cli_ext.command("ext_html_cls_ptd")
def consumirFilaExtracaoHtmlClassificacaoPartida():
    """"
    Inicia o consumidor da fila pra extração do HTML da classificação de uma competição da partida
    """
    extracao_partida_classificacao.iniciar()
