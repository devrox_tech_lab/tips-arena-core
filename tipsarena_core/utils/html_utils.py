import re
from datetime import datetime

import requests
from bs4 import BeautifulSoup

from tipsarena_core.services import log_service as log


def obterHtml(url: str):
    try:
        page = requests.get(url)
        documento_html = BeautifulSoup(page.content, "html.parser")
        page.close()
        return documento_html
    except Exception as e:
        log.ERRO("Não foi possível obter o HTML da página:{}.".format(url), e.args)
        return None


def converterTextoParaHtml(texto: str):
    try:
        dados_html = BeautifulSoup(texto, "html.parser")
        return dados_html
    except Exception as e:
        log.ERRO("Não foi possível converter o texto para HTML.", e.args)
        return None


def obterIdPartida(html) -> [str]:
    CSS_TAGS_SCRIPT = "script:not(src)"
    tagsScript = html.select(CSS_TAGS_SCRIPT)
    regex = re.compile('"event_id_c":(.*?),')
    for tag in tagsScript:
        resultado = regex.search(str(tag.string))
        if (resultado):
            return resultado.groups()[0].replace('"', "")


def obterDadosCabecalho(html):
    try:
        CSS_LINKS_CABECALHO = "h2.breadcrumb a"
        CSS_TEXTO_CABECALHO = ".teamHeader__text"
        cabecalhoCompeticao = html.select(CSS_LINKS_CABECALHO)
        linksCabecalho = []

        for item in cabecalhoCompeticao:
            linksCabecalho.append(
                {"text": item.text, "href": item.attrs["href"]})

        textoCabecalho = html.select(CSS_TEXTO_CABECALHO)

        if len(textoCabecalho) > 0:
            linksCabecalho.append(
                {"text": textoCabecalho[0].text, "href": "#"})

        return linksCabecalho
    except Exception as e:
        log.ERRO("Não foi possível obter dados do cabeçalho.", e.args)
        return None


def obterUrlAtributoOnClick(onclick):
    try:
        url = onclick.split("(")[1].split(")")[0].replace("'", "")
        return url
    except Exception as e:
        log.ERRO("Não foi possível extrair URL do atributo onclick.", e.args)
        return ""


def incluirMetadadosHtml(html: str, metadados: dict):
    documentoHtml = converterTextoParaHtml(html)
    tagBody = documentoHtml.select_one("body")
    tagMetadados = documentoHtml.select_one("metadados")

    if tagMetadados is None:
        tagMetadados = documentoHtml.new_tag("metadados")

    tagDados = documentoHtml.new_tag("dados")
    tagDados.attrs['timestamp'] = datetime.timestamp(datetime.now())
    for chave in metadados:
        tagDados.attrs[chave] = metadados[chave]

    tagMetadados.insert(0, tagDados)
    tagBody.insert(0, tagMetadados)

    return documentoHtml


def obterMetadosHtml(html: str) -> dict:
    try:
        metadados = {}
        documentoHtml = converterTextoParaHtml(html)
        tagMetadados = documentoHtml.select("metadados>dados")

        listaMetadados = [dados.attrs for dados in tagMetadados]
        for meta in listaMetadados:
            metadados = {**metadados, **meta}
        return metadados
    except Exception as e:
        metadados
