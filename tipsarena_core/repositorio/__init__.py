import os

from bson import ObjectId
from pymongo import MongoClient, UpdateOne

from tipsarena_core.exceptions import database_erros
from tipsarena_core.services import log_service as log

AMBIENTE = os.getenv("TA_ENV")
cliente = MongoClient(os.getenv("TA_MONGODB_URI"))
database = cliente.get_database(f"tips_arena_{AMBIENTE.lower()}")


def inserirDocumento(nomeColecao: str, documento: any):
    try:
        doc = obterDocumentoNormalizado(documento)
        return database[nomeColecao].insert_one(doc)
    except Exception as e:
        log.ERRO("Não foi possível inserir documento na coleção '{}' **{}**.".format(nomeColecao, documento),
                 e.args)
        raise database_erros.InserirDocumentoError(
            message="Não foi possível inserir documento na coleção '{}' **{}**.".format(nomeColecao, documento))


def atualizarDocumento(nomeColecao: str, documento: dict):
    try:
        doc = obterDocumentoNormalizado(documento)
        queryUpdate = {"_id": doc["_id"]}
        return database[nomeColecao].update_one(queryUpdate, {"$set": doc})

    except Exception as e:
        log.ERRO("Não foi possível atualizar documento na coleção '{}' **{}**.".format(nomeColecao, documento),
                 e.args)
        raise database_erros.AtualizarDocumentoError(
            message="Não foi possível atualizar documento na coleção '{}' **{}**.".format(nomeColecao, documento))


def inserirOuAtualizarDocumento(nomeColecao: str, documento: dict, filtro: dict, valoresPadrao={}):
    try:
        doc = obterDocumentoNormalizado(documento)

        for key in valoresPadrao:
            if key in documento:
                del documento[key]

        return database[nomeColecao].update_one(filtro, {"$set": doc, "$setOnInsert": valoresPadrao}, upsert=True)

    except Exception as e:
        log.ERRO(f"Não foi possível inserir documento na coleção '{nomeColecao}' **{documento}**.", e.args)
        raise database_erros.InserirOuAtualizarDocumentoError(
            message="Erro ao inserir ou atualizar documento. [{}]".format(e.args))


def inserirVariosDocumentosCasoNaoExistam(nomeColecao, listaDocumentos, campoFiltro):
    try:
        listaAtualizacao = []
        for documento in listaDocumentos:
            doc = obterDocumentoNormalizado(documento)
            listaAtualizacao.append(UpdateOne({campoFiltro: doc.get(campoFiltro)}, {"$set": doc}, upsert=True))

        return database[nomeColecao].bulk_write(listaAtualizacao, ordered=False)
    except Exception as e:
        log.ERRO(f"Não foi possível inserir {len(listaDocumentos)} documentos na coleção '{nomeColecao}'.", e.args)
        database_erros.InserirOuAtualizarDocumentosEmLoteError(
            message=f"Não foi possível inserir {len(listaDocumentos)} "
                    f"documentos na coleção '{nomeColecao}' [{e.args}].")


def deletarDocumento(nomeColecao: str, id: str):
    try:
        filtro = {"_id": ObjectId(id)}
        return database[nomeColecao].delete_one(filtro)
    except Exception as e:
        log.ERRO("Não foi possível deletar documento com id '{}' da coleção '{}'."
                 .format(id, nomeColecao), e.args)

        raise database_erros.DeletarDocumentoError(
            message=f"Não foi possível deletar documento com id '{id}' da coleção '{nomeColecao}'.")


def listarDocumentos(nomeColecao: str, filtros={}, ordenacao=[], limite=0, offset=0):
    try:
        cursor = database[nomeColecao].find(filtros)

        if ordenacao != []:
            cursor.sort(ordenacao)

        if limite > 0:
            cursor.limit(limite)

        if offset > 0:
            cursor.skip(offset)

        return cursor
    except Exception as e:
        log.ERRO(f"Não foi possível listar documentos da coleção '{nomeColecao}'.", e.args)
        raise database_erros.ListarDocumentosError(
            message=f"Não foi possível listar documentos da coleção '{nomeColecao}'.")


def obterDocumentoPorId(nomeColecao: str, id: str, converterParaObjectId=True):
    try:
        idBusca = ObjectId(id) if converterParaObjectId else id
        return database[nomeColecao].find_one({"_id": idBusca})
    except Exception as e:
        log.ERRO(f"Não foi possível encontrar o documento com o ID '{id}' da coleção '{nomeColecao}'.", e.args)
        raise database_erros.ObterDocumentoError(
            message=f"Não foi possível encontrar o documento com o ID '{id}' da coleção '{nomeColecao}'.")


def obterDocumentoPorChave(nomeColecao: str, chave: dict):
    try:
        return database[nomeColecao].find_one(chave)
    except Exception as e:
        log.ERRO(f"Não foi possível encontrar o documento com a chave '{chave}' da coleção '{nomeColecao}'.")
        raise database_erros.ObterDocumentoError(
            message=f"Não foi possível encontrar o documento com a chave '{chave}' da coleção '{nomeColecao}'.")


def obterDocumentoNormalizado(documento):
    if (hasattr(documento, "__dict__")):
        return documento.__dict__
    else:
        return documento


def pesquisarTexto(nomeColecao: str, texto: str, limite=10):
    try:
        cursor = database[nomeColecao].find({"$text": {"$search": texto}}).limit(limite)
        return list(cursor)
    except Exception as e:
        log.ERRO("Erro ao pesquisar texto '{}' na coleção [{}]".format(texto, nomeColecao), e.args)
        raise database_erros.PesquisaTextoError(
            message="Erro ao pesquisar texto '{}' na coleção [{}]".format(texto, nomeColecao))


def obterTotalDocumentos(nomeColecao: str, filtros={}):
    try:
        return database[nomeColecao].estimated_document_count()
    except Exception as e:
        log.ERRO("Não foi possível obter o total de documentos da coleção '{}'. [{}]", e.args)
        raise database_erros.ObterDocumentoError(
            message=f"Não foi possível obter o total de documentos da coleção '{nomeColecao}'.")


def listarColecoes():
    return database.list_collection_names()


def excluirColecao(nomeColecao: str):
    try:
        database[nomeColecao].drop()
    except Exception as e:
        log.ERRO("Erro ao excluir coleção {}.".format(nomeColecao), e.args)


def testarConexao() -> (bool, str):
    """"
    Realiza um teste de conexão com o servidor de banco de dados
    """
    try:
        info = cliente.server_info()
        return (True, f"Servidor de banco de dados está em execução. Versao:{info['version']}")
    except Exception as e:
        raise database_erros.ConexaoError(message=f"Teste de conexão falhou :(. Detalhes [{e.args}]")


def fazerAgregacao(nomeColecao: str, pipeline, explain=False):
    try:
        if explain:
            return database.command("aggregate", nomeColecao, pipeline=pipeline, explain=explain, allowDiskUse=True)

        return database[nomeColecao].aggregate(pipeline=pipeline, allowDiskUse=True)

    except Exception as e:
        log.ERRO("Erro ao executar agregação dos dados. {}".format(e.args))
        raise database.AgregarDadosError(
            message="Erro ao executar agregação dos dados na coleção {}. **{}**".format(nomeColecao, e.args))


def listarOperacoesCorrentes():
    with cliente.admin.aggregate([{"$currentOp": {}}]) as cursor:
        for operation in cursor:
            log.INFO("[{}] [{}]".format(operation["opid"], operation["op"]))


def criarIndice(nomeColecao: str, indice: tuple, eUnico: bool):
    try:
        resultado = database[nomeColecao].create_index([indice], unique=eUnico)
        log.OK(f"Índice '{resultado}' criado com sucesso na coleção '{nomeColecao}'!")
    except Exception as e:
        log.ERRO(f"Erro ao criar indice [{indice[0]}] na coleção {nomeColecao}.", e.args)
        raise database_erros.CriarIndiceError(
            message=f"Erro ao criar indice [{indice[0]}] na coleção {nomeColecao}. [{e.args}]")


def obterIndicesColecao(nomeColecao: str):
    return database[nomeColecao].list_indexes()
