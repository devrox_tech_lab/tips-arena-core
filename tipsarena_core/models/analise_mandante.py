# -*- coding: utf-8 -*-

from tipsarena_core.models import ModeloBase


class AnaliseMandante(ModeloBase):
    classificacao: dict
    golsMarcadosComoMandante: int
    golsSofridosComoMandante: int
    forcaOfensiva: float
    forcaDefensiva: float
