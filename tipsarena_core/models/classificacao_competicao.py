# -*- coding: utf-8 -*-

from tipsarena_core.models import ModeloBase


class Classificacao(ModeloBase):
    posicao: int
    nomeEquipe: str
    urlEquipe: str
    jogos: int
    vitorias: int
    empates: int
    derrotas: int
    golsMarcados: int
    golsSofridos: int
    pontos: int
    forma: list
