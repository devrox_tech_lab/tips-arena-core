# -*- coding: utf-8 -*-
from datetime import datetime
from typing import Optional

from tipsarena_core.models import ModeloBase


class EdicaoCompeticao(ModeloBase):
    id: str
    url: str
    urlLogo: Optional[str]
    temporada: str
    inicioTemporada: int
    fimTemporada: int
    idCompeticao: str
    nomeCompeticao: str
    nomePais: str
    equipeVencedora: Optional[dict]
    emAndamento: bool
    dataCadastro: Optional[datetime]
    dataAtualizacao: Optional[datetime]
