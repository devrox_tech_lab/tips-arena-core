# -*- coding: utf-8 -*-
from datetime import datetime
from typing import Optional

from tipsarena_core.models import ModeloBase


class Competicao(ModeloBase):
    id: str
    nome: str
    url: str
    idPais: str
    nomePais: str
    dataCadastro: Optional[datetime]
    dataAtualizacao: Optional[datetime] = datetime.now()
