# -*- coding: utf-8 -*-

from tipsarena_core.models import ModeloBase


class AnaliseVisitante(ModeloBase):
    classificacao: dict
    golsMarcadosComoVisitante: int
    golsSofridosComoVisitante: int
    forcaDefensiva: float
    forcaOfensiva: float
