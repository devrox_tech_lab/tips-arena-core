# -*- coding: utf-8 -*-
from datetime import datetime
from typing import Optional

from bson import ObjectId

from tipsarena_core.models import ModeloBase
from tipsarena_core.models.analise_competicao import AnaliseCompeticao
from tipsarena_core.models.analise_mandante import AnaliseMandante
from tipsarena_core.models.analise_visitante import AnaliseVisitante


class ExpectativaGols(ModeloBase):
    id: Optional[ObjectId]
    idPartida: Optional[str]
    idEquipeMandante: Optional[str]
    idEquipeVisitante: Optional[str]
    analiseCompeticao: AnaliseCompeticao
    analiseMandante: AnaliseMandante
    analiseVisitante: AnaliseVisitante
    expectativaDeGolMandante: float
    expectativaDeGolVisitante: float
    dataCadastro: Optional[datetime] = datetime.now()
    dataAtualizacao: Optional[datetime] = datetime.now()
