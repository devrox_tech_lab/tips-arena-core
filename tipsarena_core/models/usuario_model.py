from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class Usuario(BaseModel):
    nome: str
    cpf: str
    eMail: str
    eMailSecundario: str
    celular: str
    valorAssinatura: float
    origem: str
    idDiretorioDrive: str
    idSpreadsheet: str
    idCalendario: str
    statusConfiguracao: str
    numeroCRP: str
    status: str
    ufCRP: str
    cep: str
    logradouro: str
    numeroEndereco: str
    complementoEndereco: str
    bairro: str
    cidade: str
    uf: str
    licenca: str
    senha: str
    dataCadastro: Optional[datetime]
    dataAtualizacao: Optional[datetime]
