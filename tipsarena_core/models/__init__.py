from datetime import date, datetime

from bson import ObjectId
from pydantic import BaseConfig, BaseModel

from tipsarena_core.exceptions import model_erros


class ModeloBase(BaseModel):
    class Config(BaseConfig):
        arbitrary_types_allowed = True
        allow_population_by_field_name = True
        json_encoders = {
            datetime: lambda dt: dt.isoformat(),
            ObjectId: lambda oid: str(oid),
        }

    @classmethod
    def converter_para_objeto(cls, dados: dict):
        try:
            if not dados:
                raise model_erros.ConverterParaObjetoError(
                    message=f"Erro ao converter dicionário para modelo. Dados não informados.")

            id_objeto = dados.pop('_id', None)
            return cls(**dict(dados, id=id_objeto))
        except Exception as e:
            raise model_erros.ConverterParaDicionarioError(
                message=f"Erro ao converter modelo para dicionário. **{e.args}**")

    def converter_para_dicionario(self, **kwargs):
        try:
            exclude_unset = kwargs.pop('exclude_unset', False)
            by_alias = kwargs.pop('by_alias', True)

            dados = self.dict(
                exclude_unset=exclude_unset,
                by_alias=by_alias,
                **kwargs,
            )

            if '_id' not in dados and 'id' in dados:
                dados['_id'] = dados.pop('id')

            if dados['_id'] is None:
                del dados['_id']

            return dados
        except Exception as e:
            raise model_erros.ConverterParaDicionarioError(
                message=f"Erro ao converter modelo para dicionário. **{e.args}**")


def serealizador_json(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()

    raise TypeError("Type %s not serializable" % type(obj))
