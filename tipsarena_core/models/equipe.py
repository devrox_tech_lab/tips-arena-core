# -*- coding: utf-8 -*-
from datetime import datetime
from typing import Optional

from tipsarena_core.models import ModeloBase


class Equipe(ModeloBase):
    id: str
    nome: str
    pais: dict
    urlEscudo: Optional[str]
    url: str
    dataCadastro: Optional[datetime]
    dataAtualizacao: Optional[datetime]
