# -*- coding: utf-8 -*-
from datetime import datetime
from typing import Optional

from tipsarena_core.models import ModeloBase


class ItemExtracao(ModeloBase):
    uuid: str
    url: str
    hashUrl: str
    tipo: str
    dataHora: Optional[datetime] = datetime.now()
    extras: Optional[dict] = {}
