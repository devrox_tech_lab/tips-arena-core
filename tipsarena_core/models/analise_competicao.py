# -*- coding: utf-8 -*-

from tipsarena_core.models import ModeloBase


class AnaliseCompeticao(ModeloBase):
    totalPartidas: int
    totalGolsFeitosMandante: int
    totalGolsSofridosMandante: int
    totalGolsFeitosVisitantes: int
    totalGolsSofridosVisitantes: int
