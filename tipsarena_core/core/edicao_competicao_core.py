# -*- coding: utf-8 -*-
from datetime import datetime

from tipsarena_core import repositorio
from tipsarena_core.models.edicao_competicao import EdicaoCompeticao

NOME_COLECAO = "edicoes_competicao"

OPCOES_FILTRO = {
    "pais": []
}

OPCOES_ORDENACAO = [{
    "dataCadastro": -1,
    "dataAtualizacao": -1
}]


def salvarEdicaoCompeticao(edicao: EdicaoCompeticao):
    edicao.dataAtualizacao = datetime.utcnow()
    filtro = {"_id": edicao.id}
    return repositorio.inserirOuAtualizarDocumento(NOME_COLECAO,
                                                   edicao.converter_para_dicionario(),
                                                   filtro,
                                                   {"dataCadastro": datetime.utcnow()})


def obterCompeticaoPorId(id):
    return repositorio.obterDocumentoPorId(NOME_COLECAO, id)


def obterInicioFimTemporada(temporada: str):
    array_temporada = temporada.split("-")
    anoInicio= array_temporada[0]
    anoFim = anoInicio

    if len(array_temporada) == 2:
        anoFim = array_temporada[1]

    return int(anoInicio), int(anoFim)
