# -*- coding: utf-8 -*-

from datetime import datetime

from tipsarena_core import repositorio
from tipsarena_core.models.pais import Pais

NOME_COLECAO = "paises"

OPCOES_FILTRO = {
    "pais": []
}

OPCOES_ORDENACAO = [{
    "dataCadastro": -1,
    "dataAtualizacao": -1
}]


def salvarPais(pais: Pais):
    pais.dataAtualizacao = datetime.utcnow()
    filtro = {"_id": pais.id}
    return repositorio.inserirOuAtualizarDocumento(NOME_COLECAO,
                                                   pais.converter_para_dicionario(),
                                                   filtro,
                                                   {"dataCadastro": datetime.utcnow()})


def obterPaisPorId(id):
    return repositorio.obterDocumentoPorId(NOME_COLECAO, id)
