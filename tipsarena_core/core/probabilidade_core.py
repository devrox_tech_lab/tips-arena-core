import math
from datetime import datetime

from tipsarena_core import repositorio
from tipsarena_core.models.analise_competicao import AnaliseCompeticao
from tipsarena_core.models.analise_mandante import AnaliseMandante
from tipsarena_core.models.analise_visitante import AnaliseVisitante
from tipsarena_core.models.classificacao_competicao import Classificacao
from tipsarena_core.models.expectativa_gols import ExpectativaGols
from tipsarena_core.utils import logUtils as log

NOME_COLECAO = "expectativas_gol"


def calcularExpectativaDeGols(urlEquipeMandante: str, urlEquipeVisitante: str, classificacaoGeral: list[Classificacao],
                              classificacaoCasa: list[Classificacao], classificacaoFora: list[Classificacao]):
    totalPartidasCompeticao = 0
    for classificacao in classificacaoGeral:
        totalPartidasCompeticao += classificacao.jogos

        if urlEquipeMandante.find(classificacao.urlEquipe) > -1:
            classificacaoMandante = classificacao

        if urlEquipeVisitante.find(classificacao.urlEquipe) > -1:
            classificacaoVisitante = classificacao

    golsMarcadosComoMandante = 0
    golsSofridosComoMandante = 0
    totalPartidasComoMandante = 0
    totalGolsFeitosMandanteCompeticao = 0
    totalGolsSofridosMandanteCompeticao = 0

    for classificacao in classificacaoCasa:
        totalGolsFeitosMandanteCompeticao += classificacao.golsMarcados
        totalGolsSofridosMandanteCompeticao += classificacao.golsSofridos

        if urlEquipeMandante.find(classificacao.urlEquipe) > -1:
            golsMarcadosComoMandante += classificacao.golsMarcados
            golsSofridosComoMandante += classificacao.golsSofridos
            totalPartidasComoMandante += classificacao.jogos

    golsMarcadosComoVisitante = 0
    golsSofridosComoVisitante = 0
    totalPartidasComoVisitante = 0
    totalGolsFeitosVisitantesCompeticao = 0
    totalGolsSofridosVisitantesCompeticao = 0

    for classificacao in classificacaoFora:
        totalGolsFeitosVisitantesCompeticao += classificacao.golsMarcados
        totalGolsSofridosVisitantesCompeticao += classificacao.golsSofridos

        if urlEquipeVisitante.find(classificacao.urlEquipe) > -1:
            golsMarcadosComoVisitante += classificacao.golsMarcados
            golsSofridosComoVisitante += classificacao.golsSofridos
            totalPartidasComoVisitante += classificacao.jogos

    forcaOfensivaMandante = (golsMarcadosComoMandante / totalPartidasComoMandante) / (
        totalGolsFeitosMandanteCompeticao / totalPartidasCompeticao)

    forcaDefensivaVisitante = (golsSofridosComoVisitante / totalPartidasComoVisitante) / (
        totalGolsSofridosVisitantesCompeticao / totalPartidasCompeticao)

    forcaOfensivaVisitante = (golsMarcadosComoVisitante / totalPartidasComoVisitante) / (
        totalGolsFeitosVisitantesCompeticao / totalPartidasCompeticao)

    forcaDefensivaMandante = (golsSofridosComoMandante / totalPartidasComoMandante) / (
        totalGolsSofridosMandanteCompeticao / totalPartidasCompeticao)

    expectativaGolsMandante = forcaOfensivaMandante * forcaDefensivaVisitante * (
        totalGolsFeitosMandanteCompeticao / totalPartidasCompeticao)

    expectativaGolsVisitante = forcaOfensivaVisitante * forcaDefensivaMandante * (
        totalGolsFeitosVisitantesCompeticao / totalPartidasCompeticao)

    analiseCompeticao = AnaliseCompeticao(
        **{
            "totalPartidas": totalPartidasCompeticao,
            "totalGolsFeitosMandante": totalGolsFeitosMandanteCompeticao,
            "totalGolsSofridosMandante": totalGolsSofridosMandanteCompeticao,
            "totalGolsFeitosVisitantes": totalGolsFeitosVisitantesCompeticao,
            "totalGolsSofridosVisitantes": totalGolsSofridosVisitantesCompeticao,
        })

    analiseMandante = AnaliseMandante(
        **{
            "classificacao": classificacaoMandante,
            "golsMarcadosComoMandante": golsMarcadosComoMandante,
            "golsSofridosComoMandante": golsSofridosComoMandante,
            "forcaOfensiva": forcaOfensivaMandante,
            "forcaDefensiva": forcaDefensivaMandante
        })

    analiseVisitante = AnaliseVisitante(
        **{
            "classificacao": classificacaoVisitante,
            "golsMarcadosComoVisitante": golsMarcadosComoVisitante,
            "golsSofridosComoVisitante": golsSofridosComoVisitante,
            "forcaDefensiva": forcaDefensivaVisitante,
            "forcaOfensiva": forcaOfensivaVisitante
        })

    return ExpectativaGols(
        **{
            "analiseCompeticao": analiseCompeticao,
            "analiseMandante": analiseMandante,
            "analiseVisitante": analiseVisitante,
            "expectativaDeGolMandante": expectativaGolsMandante,
            "expectativaDeGolVisitante": expectativaGolsVisitante
        })


def gerarMatrizProbabilidadesDeGols(expectativaGolsMandante: float, expectativaGolsVisitante: float):
    probabilidades = [[0.] * 11 for _ in range(11)]
    for k in range(11):
        for n in range(11):
            probK = calcularProbabilidadeDeGol(expectativaGolsMandante, k)
            probN = calcularProbabilidadeDeGol(expectativaGolsVisitante, n)
            probabilidades[k][n] = probK * probN

    return probabilidades


def calcularProbabilidadeDeGol(expectativaGols: float, numeroGols: int) -> float:
    return (math.pow(math.e, -expectativaGols) * math.pow(expectativaGols, numeroGols)) / math.factorial(numeroGols)


def salvarExpectativasDeGol(expectativas: ExpectativaGols):
    try:
        expectativas.dataAtualizacao = datetime.utcnow()
        filtro = {"idPartida": expectativas.idPartida}

        return repositorio.inserirOuAtualizarDocumento(NOME_COLECAO,
                                                       expectativas.converter_para_dicionario(), filtro,
                                                       {"dataCadastro": datetime.utcnow()})
    except Exception as e:
        log.imprimirMensagem("ERRO", "Não foi possível salvar partida [{}]".format(expectativas.idPartida), e.args)
        return None


def obterExpectativasDeGolPorPartida(idPartida: str) -> ExpectativaGols:
    doc = repositorio.obterDocumentoPorChave(NOME_COLECAO, {"idPartida": idPartida})
    return ExpectativaGols.converter_para_objeto(doc)


def calcularProbalidadesResultado(expectativaGolsMandante: float, expectativasGolsVisitante: float):
    matrizProbs = gerarMatrizProbabilidadesDeGols(expectativaGolsMandante,
                                                  expectativasGolsVisitante)
    probMandante = 0.
    probEmpate = 0.
    probVisitante = 0.

    for k in range(len(matrizProbs)):
        for n in range(len(matrizProbs[k])):
            if n == k:
                probEmpate += matrizProbs[k][n]
            elif k > n:
                probMandante += matrizProbs[k][n]
            else:
                probVisitante += matrizProbs[k][n]

    return {
        "mandante": probMandante,
        "empate": probEmpate,
        "visitante": probVisitante
    }
