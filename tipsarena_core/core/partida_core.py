# -*- coding: utf-8 -*-

from datetime import datetime

from tipsarena_core import repositorio
from tipsarena_core.core import aposta_core
from tipsarena_core.models.partida import Partida
from tipsarena_core.utils import logUtils as log

NOME_COLECAO = 'partidas'
OPCOES_FILTRO = {
    "idEdicaoCompeticao": "",
    "status": [],
    "dataHora": [],
    "dataHoraInicio": "",
    "dataHoraFim": ""
}

OPCOES_ORDENACAO = [{
    "dataHora": -1,
    "dataCadastro": -1,
    "dataAtualizacao": -1
}]


def criarIndicesPaciente():
    indicesAplicacao = [
        (("cpf", repositorio.ASCENDING), True),
        (("eMail", repositorio.ASCENDING), True),
        (("celular", repositorio.ASCENDING), True)
    ]

    for campo, eUnico in indicesAplicacao:
        repositorio.criarIndice(NOME_COLECAO, campo, eUnico)


def salvarPartida(partida: Partida):
    partida.dataAtualizacao = datetime.utcnow()
    filtro = {"_id": partida.id}

    return repositorio.inserirOuAtualizarDocumento(NOME_COLECAO,
                                                   partida.converter_para_dicionario(), filtro,
                                                   {"dataCadastro": datetime.utcnow()})


def obterPartidaPorId(id):
    doc = repositorio.obterDocumentoPorId(NOME_COLECAO, id, False)
    return Partida.converter_para_objeto(doc)


def listPartidas(filtros={}, ordenacao=[], limite=0, offset=0):
    filtroDataHora = {}

    if filtros != {}:
        if filtros["idEdicaoCompeticao"] == "":
            del filtros["idEdicaoCompeticao"]

        if filtros["dataHoraInicio"] != "":
            filtroDataHora["$gte"] = filtros["dataHoraInicio"]

        del filtros["dataHoraInicio"]

        if filtros["dataHoraFim"] != "":
            filtroDataHora["$lte"] = filtros["dataHoraFim"]

        del filtros["dataHoraFim"]

        if len(filtros["status"]) == 0:
            del filtros["status"]
        else:
            filtros["status"] = {"$in": filtros["status"]}

        if filtros["dataHora"] != {}:
            filtros["dataHora"] = filtroDataHora

    cursor = repositorio.listarDocumentos(NOME_COLECAO, filtros, [("dataHora", -1)], limite, offset)
    return [Partida.converter_para_objeto(doc) for doc in cursor]


def analisarAlteracoesPartida(partida: Partida, partidaAtualizada: Partida):
    try:
        listaAlteracoes = []
        partida = partida.__dict__
        partidaAtualizada = partidaAtualizada.__dict__

        for key in partida:
            if partida[key] != partidaAtualizada[key]:
                listaAlteracoes.append(
                    {"campo": key,
                     "valorAnterior": partida[key],
                     "valorNovo": partidaAtualizada[key]})

        return listaAlteracoes
    except Exception as e:
        log.imprimirMensagem("ERRO",
                             "Não foi possível analisar alterações na partida [{}].".format(partida.get("url")),
                             e.args)
        return None


def processarAlteracoesPartida(partida: Partida, alteracoes):
    try:
        for alteracao in alteracoes:
            if alteracao["campo"] == "status":
                if alteracao["valorNovo"] == Partida.Status.FINALIZADO.name:
                    aposta_core.finalizarApostasPartida(partida)
            if alteracao["campo"] == "placarFinal":
                aposta_core.finalizarApostasPartida(partida)

    except Exception as e:
        log.imprimirMensagem("ERRO",
                             "Não foi possível processar alterações na partida [{}].".format(partida.get("url")),
                             e.args)
