# -*- coding: utf-8 -*-

from datetime import datetime

from tipsarena_core import repositorio
from tipsarena_core.models.competicao import Competicao

NOME_COLECAO = "competicoes"

OPCOES_FILTRO = {
    "pais": []
}

OPCOES_ORDENACAO = [{
    "dataCadastro": -1,
    "dataAtualizacao": -1
}]


def salvarCompeticao(competicao: Competicao):
    competicao.dataAtualizacao = datetime.utcnow()
    filtro = {"_id": competicao.id}

    return repositorio.inserirOuAtualizarDocumento(NOME_COLECAO,
                                                   competicao.converter_para_dicionario(),
                                                   filtro,
                                                   {"dataCadastro": datetime.utcnow()})


def obterCompeticaoPorId(id):
    return repositorio.obterDocumentoPorId(NOME_COLECAO, id)
