from tipsarena_core.exceptions import Error


class ConverterParaObjetoError(Error): pass


class ConverterParaDicionarioError(Error): pass
