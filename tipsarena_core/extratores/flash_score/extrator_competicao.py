# -*- coding: utf-8 -*-
from datetime import datetime

from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.item_processamento import ItemProcessamento
from tipsarena_core.services import auth_service
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import hash_utils, html_utils, string_utils


def extrairHtmlCompeticoesPais(urlPais: str, metadados={}):
    try:
        html = html_utils.obterHtml(urlPais)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        hashUrlPais = hash_utils.gerar_hash(urlPais)

        htmlFinal = html_utils.incluirMetadadosHtml(str(html), metadados)
        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), {"url_pais": urlPais,
                                                                     "hash_url_pais": hashUrlPais,
                                                                     "tipo_extracao": "EXT_HTML_LISTA_COMPETICOES_PAIS"
                                                                     })

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"cmp-ps-{hashUrlPais.lower()}.html",
                "tipo": "PROC_HTML_LISTA_COMPETICOES_PAIS",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "url_pais": urlPais,
                           "hash_url_pais": hashUrlPais}
            })

    except Exception as e:
        log.ERRO(f"Erro ao obter lista de competições do país [{urlPais}]", e.args)
        return None


def extrairHtmlCompeticao(urlCompeticao: str, metadados={}):
    try:
        html = html_utils.obterHtml(navegador_web.URL_BASE + urlCompeticao)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()
        hashUrlCompeticao = hash_utils.gerar_hash(urlCompeticao)

        htmlFinal = html_utils.incluirMetadadosHtml(str(html), metadados)
        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), {"url_competicao": urlCompeticao,
                                                                     "url_competicao_hash": hashUrlCompeticao,
                                                                     "tipo_extracao": "EXT_HTML_COMPETICAO"})

        return ItemProcessamento(
            {
                "uuid": uuid,
                "nomeArquivo": f"cmp-{hashUrlCompeticao}.html",
                "tipo": "PROC_HTML_COMPETICAO",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "url_competicao": urlCompeticao,
                           "hash_url_ompeticao": hashUrlCompeticao
                           }
            })

    except Exception as e:
        log.ERRO(f"Erro ao extrair html da competição [{urlCompeticao}]", e.args)
        return None


def extrairHtmlEdicoesCompeticao(urlCompeticao: str, metadados={}):
    try:
        urlEdicoes = f"{urlCompeticao}arquivo/"
        html = html_utils.obterHtml(urlEdicoes)

        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        hashUrlEdicoes = hash_utils.gerar_hash(urlEdicoes)

        htmlFinal = html_utils.incluirMetadadosHtml(str(html), metadados)
        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), {"url_edicoes": urlEdicoes,
                                                                     "hash_url_edicoes": hashUrlEdicoes,
                                                                     "tipo_extracao": "EXT_HTML_LISTA_EDICOES_COMPETICAO",
                                                                     "url_competicao": urlCompeticao,
                                                                     "hash_url_competicao": hash_utils.gerar_hash(
                                                                         urlCompeticao)
                                                                     })

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"edc-cmp-{hashUrlEdicoes.lower()}.html",
                "tipo": "PROC_HTML_LISTA_EDICOES_COMPETICAO",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "url_edicoes": urlEdicoes,
                           "hash_url_edicoes": hashUrlEdicoes,
                           "url_competicao": urlCompeticao,
                           "hash_url_competicao": hash_utils.gerar_hash(
                               urlCompeticao)
                           }
            })


    except Exception as e:
        log.ERRO(f"Erro ao extrair html de edições da competição [{urlCompeticao}]", e.args)
        return None
