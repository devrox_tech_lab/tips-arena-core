# -*- coding: utf-8 -*-
from datetime import datetime

from tipsarena_core.extratores.flash_score import extrator_partida
from tipsarena_core.models.item_processamento import ItemProcessamento
from tipsarena_core.services import auth_service
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import hash_utils, html_utils
from tipsarena_core.utils import logUtils as log
from tipsarena_core.utils import string_utils


def extrairHtmlPartidasFinalizadasEdicaoCompeticao(urlEdicao: str, metadados={}):
    try:
        urlPartidas = f"{urlEdicao}resultados/"
        metadados["url_edicao"] = urlEdicao
        metadados["hash_url_edicao"] = hash_utils.gerar_hash(urlEdicao)

        return extrator_partida.extrairHtmlPartidasEdicaoCompeticao(urlPartidas, metadados)
    except Exception as e:
        log.ERRO(f"Não foi possível extrair HTML lista de IDS de partidas da edição da competicão {urlEdicao}.", e.args)
        return None


def extrairHtmlPartidasAgendadasEdicaoCompeticao(urlEdicao: str, metadados={}):
    try:
        urlPartidas = f"{urlEdicao}calendario/"
        metadados["url_edicao"] = urlEdicao
        metadados["hash_url_edicao"] = hash_utils.gerar_hash(urlEdicao)

        return extrator_partida.extrairHtmlPartidasEdicaoCompeticao(urlPartidas, metadados)

    except Exception as e:
        log.ERRO(f"Não foi possível extrair HTML lista de IDS de partidas da edição da competicão {urlEdicao}.", e.args)
        return None


def extrairHtmlEdicaoCompeticao(urlEdicao: str, metadados={}):
    try:
        documentoHtml = html_utils.obterHtml(urlEdicao)

        urlHshEdicao = hash_utils.gerar_hash(urlEdicao)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        htmlFinal = html_utils.incluirMetadadosHtml(str(documentoHtml), metadados)
        html_utils.incluirMetadadosHtml(str(documentoHtml), {"url_edicao": urlEdicao,
                                                             "hash_url_edicao": urlHshEdicao,
                                                             "tipo_extracao": "EXT_HTML_EDICAO_COMPETICAO"})

        return ItemProcessamento(
            **{"uuid": uuid,
               "nomeArquivo": f"edc-{urlHshEdicao.lower()}.html",
               "tipo": "EXT_HTML_EDICAO_COMPETICAO",
               "dataHora": dataHoraExtracao,
               "extras": {"html": string_utils.limparString(str(htmlFinal)),
                          "urlEdicao": urlEdicao,
                          "hashUrlEdicao": urlHshEdicao}
               })

    except Exception as e:
        log.ERRO(f"Não foi possível obter html da edição da competição {urlEdicao}.", e.args)
        return None
