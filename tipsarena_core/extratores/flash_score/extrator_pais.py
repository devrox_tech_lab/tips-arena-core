# -*- coding: utf-8 -*-
from datetime import datetime

from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.item_processamento import ItemProcessamento
from tipsarena_core.services import auth_service
from tipsarena_core.utils import hash_utils, html_utils, string_utils


def extrairHtmlPaises():
    try:
        CSS_LISTA_PAISES = "body"
        CSS_VERIFICAR_CARREGAMENTO = "#category-left-menu"
        CSS_LISTAR_MAIS_PAISES = "[class*=itemMore]"

        urlPaises = f"{navegador_web.URL_BASE}/futebol/"
        hashUrlPaises = hash_utils.gerar_hash(urlPaises)
        navegador_web.navegar(urlPaises)

        elementoListaMaisPaises = navegador_web.obterElementoAposCarregamento(CSS_LISTAR_MAIS_PAISES)

        elementoListaMaisPaises.click()

        navegador_web.obterElementoAposCarregamento(CSS_VERIFICAR_CARREGAMENTO)
        htmlListaPaises = navegador_web.obterElementoAposCarregamento(CSS_LISTA_PAISES)

        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        metadados = {
            "url_paises": urlPaises,
            "has_url_paises": hashUrlPaises,
            "tipo_extracao": "EXT_HTML_LISTA_PAISES"
        }

        htmlFinal = html_utils.incluirMetadadosHtml(htmlListaPaises.get_attribute("outerHTML"), metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"ps-{hashUrlPaises.lower()}.html",
                "tipo": "EXT_HTML_LISTA_PAISES",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlPaises": urlPaises,
                           "hashUrlPaises": hashUrlPaises}
            })
    finally:
        navegador_web.finalizarNavegadorWeb()


if __name__ == "__main__":
    extrairHtmlPaises()
