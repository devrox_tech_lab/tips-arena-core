# -*- coding: utf-8 -*-
from datetime import datetime

from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.item_processamento import ItemProcessamento
from tipsarena_core.services import auth_service
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import hash_utils, html_utils, string_utils


def extrairHtmlEquipesEdicaoCompeticao(urlEdicao: str, metadados={}):
    try:
        CSS_TABELA_CLASSIFICACAO = "#tournament-table-tabs-and-content"

        urlEquipes = f"{urlEdicao}classificacao/"
        navegador_web.navegar(urlEquipes)

        navegador_web.obterElementoAposCarregamento(CSS_TABELA_CLASSIFICACAO)
        html = navegador_web.obterElementoAposCarregamento("body")

        hashUrlEdicao = hash_utils.gerar_hash(urlEquipes)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        htmlFinal = html_utils.incluirMetadadosHtml(html.get_attribute('outerHTML'), metadados)
        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), {"url_equipes": urlEquipes,
                                                                     "hasg_url_equipes": hashUrlEdicao,
                                                                     "tipo_extracao": "EXT_HTML_EQUIPES_EDICAO_COMPETICAO"
                                                                     })

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"eqp-edc-{hashUrlEdicao.lower()}.html",
                "tipo": "PROC_EXT_HTML_EQUIPES_EDICAO_COMPETICAO",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlEdicao": urlEdicao,
                           "hashUrlEdicao": hashUrlEdicao,
                           }
            })

    except Exception as e:
        log.ERRO(f"Não foi possível extrair html de equipes da edição da competição [{urlEdicao}]", e.args)
        return None


def extrairHtmlEquipe(urlEquipe: str, metadados={}):
    try:
        documentoHtml = html_utils.obterHtml(urlEquipe)

        hasUrlEquipe = hash_utils.gerar_hash(urlEquipe)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        htmlFinal = html_utils.incluirMetadadosHtml(str(documentoHtml), metadados)
        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), {
            "url_equipe": urlEquipe,
            "hash_url_equipe": hasUrlEquipe,
            "tipo_extracao": "EXT_HTML_EQUIPE"
        })

        return ItemProcessamento(
            {
                "uuid": uuid,
                "nomeArquivo": f"eqp-{hasUrlEquipe.lower()}.html",
                "tipo": "PROC_HTML_EQUIPE",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlEquipe": urlEquipe,
                           "hashUrlEquipe": hasUrlEquipe}
            })

    except Exception as e:
        log.ERRO(f"Não foi possível extrair html da equipe [{urlEquipe}]", e.args)
        return None
