# -*- coding: utf-8 -*-
import time
from datetime import datetime

from selenium.webdriver.common.by import By

from tipsarena_core.enums.enum_aposta import MERCADO
from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.item_processamento import ItemProcessamento
from tipsarena_core.services import auth_service
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import hash_utils, html_utils, string_utils

CASAS_DECIMAIS = 3


def extrairHtmlPartidasEdicaoCompeticao(urlEdicao: str, metadados={}):
    try:
        CSS_LINK_LISTAR_MAIS = "a.event__more"
        CSS_LOADING = ".loadingOverlay"
        CSS_TABELA_PARTIDAS = "#live-table"

        navegador_web.navegar(urlEdicao)
        browser = navegador_web.obterNavegadorWeb()

        while len(browser.find_elements(By.CSS_SELECTOR, CSS_LINK_LISTAR_MAIS)) > 0:
            linkListarMais = browser.find_elements(By.CSS_SELECTOR, CSS_LINK_LISTAR_MAIS)[0]
            navegador_web.fecharPopupCookies()
            linkListarMais.click()
            navegador_web.aguardarCarregamentoPagina(CSS_LOADING)

        navegador_web.obterElementoAposCarregamento(CSS_TABELA_PARTIDAS)
        htmlPartidas = navegador_web.obterElementoAposCarregamento("body")


        hashUrl = hash_utils.gerar_hash(urlEdicao)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        htmlFinal = html_utils.incluirMetadadosHtml(
            f"<html>{htmlPartidas.get_attribute('outerHTML')}>/html>", metadados)

        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), {"url_partidas": urlEdicao,
                                                                     "hash_url_partidas": hashUrl,
                                                                     "tipo_extracao": "EXT_HTML_PARTIDAS_EDICAO",
                                                                     })

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"ptd-edc-{hashUrl.lower()}.html",
                "tipo": "PROC_HTML_PARTIDAS",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "url_partida": urlEdicao,
                           "hash_url_partida": hashUrl, }
            })

    except Exception as e:
        log.ERRO(f"Não foi possível extrair HTML de partidas da url {urlEdicao}.", e.args)
        return None


def extrairHtmlPartidasDia(indiceDia=0):
    CSS_LOADING = ".loadingOverlay"
    CSS_LINK_YESTERDAY = "div.calendar__navigation--yesterday"
    CSS_LINK_TOMORROW = "div.calendar__navigation--tomorrow"

    CSS_SELETOR_DATA = "div.calendar__datepicker"
    CSS_LISTA_DATAS = "div.calendar__datepicker--dates div.day"
    CSS_TABELA_PARTIDAS = "#live-table"

    try:
        url = navegador_web.URL_BASE
        navegador_web.navegar(url)
        browser = navegador_web.obterNavegadorWeb()

        botaoExibirCalendario = browser.find_element(By.CSS_SELECTOR, CSS_SELETOR_DATA)
        botaoExibirCalendario.click()
        listaDatas = browser.find_elements(By.CSS_SELECTOR, CSS_LISTA_DATAS)
        botaoExibirCalendario.click()

        posicaoMeioLista = int(len(listaDatas) / 2)

        navegador_web.aguardarCarregamentoPagina(CSS_LOADING)

        # avançar dias
        if indiceDia > 0:
            posicaoUltimaData = posicaoMeioLista + indiceDia
            for i in range(posicaoMeioLista + 1, posicaoUltimaData + 1):
                botaoIrProximaData = browser.find_element(By.CSS_SELECTOR, CSS_LINK_TOMORROW)
                botaoIrProximaData.click()
                navegador_web.aguardarCarregamentoPagina(CSS_LOADING)

        # retroceder dias
        if indiceDia < 0:
            posicaoPrimeiraData = posicaoMeioLista + indiceDia
            for i in range(posicaoPrimeiraData, posicaoMeioLista):
                botaoIrProximaData = browser.find_element(By.CSS_SELECTOR, CSS_LINK_YESTERDAY)
                botaoIrProximaData.click()
                navegador_web.aguardarCarregamentoPagina(CSS_LOADING)

        expandirPartidasCompeticao(browser)
        htmlPartidas = browser.find_element(By.CSS_SELECTOR, CSS_TABELA_PARTIDAS)

        hashUrl = hash_utils.gerar_hash(url)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()
        htmlSeletorData = browser.find_element(By.CSS_SELECTOR, CSS_SELETOR_DATA)
        dataPartidas = htmlSeletorData.text.replace("/", " ").split(" ")

        metadados = {
            "url": url,
            "hash_url": hashUrl,
            "tipo_extracao": "EXT_HTML_PARTIDAS_DIA",
            "data_partidas": f"{datetime.strftime(datetime.now(), '%Y')}-{dataPartidas[1]}-{dataPartidas[0]}",
            "dia_da_semana": dataPartidas[0]
        }

        htmlFinal = html_utils.incluirMetadadosHtml(
            f"<body>{htmlPartidas.get_attribute('outerHTML')}</body>",
            metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"ptd-dia-{datetime.strftime(datetime.now(), '%Y')}"
                               f"{dataPartidas[1]}{dataPartidas[0]}-{dataPartidas[2]}.html",
                "tipo": "PROC_HTML_PARTIDAS_DIA",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "url": url,
                           "hashUrl": hashUrl}
            })

    except Exception as e:
        log.ERRO("Não foi possível extrair HTML partidas do dia.", e.args)
        navegador_web.capturarTela()
        return None
    finally:
        navegador_web.finalizarNavegadorWeb()


def expandirPartidasCompeticao(browser):
    try:
        CSS_LINK_EXPAND_LEAGUE = "svg.event__expander--close"
        linksExpandirPartidasCompeticao = browser.find_elements(By.CSS_SELECTOR,
                                                                CSS_LINK_EXPAND_LEAGUE)

        for link in linksExpandirPartidasCompeticao:
            try:
                link.click()
            except:
                navegador_web.fecharPopupCookies()
                time.sleep(1)
                link.click()

    except Exception as e:
        log.ERRO("Não foi possível exibir todas as partidas ocultas.", e.args)


def extrairHtmlPartida(urlPartida: str, metadados={}):
    try:
        CSS_DADOS_PARTIDA = "body"
        CSS_VERIFICAR_CARREGAMENTO = "#detail"

        navegador_web.navegar(f"{urlPartida}")

        navegador_web.obterElementoAposCarregamento(CSS_VERIFICAR_CARREGAMENTO)
        htmlDadosPartida = navegador_web.obterElementoAposCarregamento(CSS_DADOS_PARTIDA)

        hashUrlPartida = hash_utils.gerar_hash(urlPartida)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        htmlFinal = html_utils.incluirMetadadosHtml(htmlDadosPartida.get_attribute("outerHTML"), {
            "url_partida": urlPartida,
            "hash_url_partida": hashUrlPartida,
            "tipo_extracao": "EXT_HTML_PARTIDA",
        })

        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"ptd-{hashUrlPartida.lower()}.html",
                "tipo": "PROC_HTML_PARTIDA",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlPartida": urlPartida,
                           "hashUrlPartida": hashUrlPartida,
                           }
            })

    except Exception as e:
        log.ERRO(f"Não foi possível extrair HTML dados partida: {urlPartida}", e.args)
        return None


def extrairHtmlClassificacao(urlPartida: str, metadados={}):
    try:
        CSS_DADOS_EQUIPES = "a.participant__participantName"
        CSS_DADOS_CLASSIFICACAO = "div.tableWrapper"

        urlClassificacaoGeral = f"{urlPartida}#/classificacao/table/overall"
        urlClassificacaoCasa = f"{urlPartida}#/classificacao/table/home"
        urlClassificacaoFora = f"{urlPartida}#/classificacao/table/away"

        navegador_web.navegar(urlClassificacaoGeral)
        htmlClassificacaoGeral = navegador_web.obterElementoAposCarregamento(CSS_DADOS_CLASSIFICACAO).get_attribute(
            "outerHTML")

        navegador_web.navegar(urlClassificacaoCasa)
        htmlClassificacaoCasa = navegador_web.obterElementoAposCarregamento(CSS_DADOS_CLASSIFICACAO).get_attribute(
            "outerHTML")

        navegador_web.navegar(urlClassificacaoFora)
        htmlClassificacaoFora = navegador_web.obterElementoAposCarregamento(CSS_DADOS_CLASSIFICACAO).get_attribute(
            "outerHTML")

        htmlEquipes = navegador_web.obterNavegadorWeb().find_elements(By.CSS_SELECTOR, CSS_DADOS_EQUIPES)
        nomeEquipeMandante = htmlEquipes[0].text
        urlEquipeMandante = htmlEquipes[0].get_attribute("href")

        nomeEquipeVisitante = htmlEquipes[1].text
        urlEquipeVisitante = htmlEquipes[1].get_attribute("href")

        hashUrlPartida = hash_utils.gerar_hash(urlPartida)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        htmlClassificacoes = f"""<body>
      <div id="classificacao_geral">{htmlClassificacaoGeral}</div>
      <div id="classificacao_casa">{htmlClassificacaoCasa}</div>
      <div id="classificacao_fora">{htmlClassificacaoFora}</div>
    </body>"""

        htmlFinal = html_utils.incluirMetadadosHtml(htmlClassificacoes, {
            "url_partida": urlPartida,
            "hash_url_partida": hashUrlPartida,
            "tipo_extracao": "EXT_HTML_CLASSIFICACAO_PARTIDA",
            "nome_equipe_mandante": nomeEquipeMandante,
            "url_equipe_mandante": f"{urlEquipeMandante}/",
            "nome_equipe_visitante": nomeEquipeVisitante,
            "url_equipe_visitante": f"{urlEquipeVisitante}/"
        })
        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlFinal), metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"ptd-cls-{hashUrlPartida.lower()}.html",
                "tipo": "PROC_HTML_PARTIDA_CLASSIFICACAO",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlPartida": urlPartida,
                           "hashUrlPartida": hashUrlPartida,
                           }
            })
    except Exception as e:
        log.ERRO("Não foi possível obter tabela de classificação da partida.", e.args)
        return None


def extrairHtmlTimelinePartida(urlPartida: str):
    try:
        CSS_DADOS_TIMELINE = "body"
        CSS_VERIFICAR_CARREGAMENTO = "div[class^=verticalSections]"

        urlTimeline = f"{navegador_web.URL_BASE}{urlPartida}#resumo-de-jogo/estatisticas-de-jogo/"
        navegador_web.navegar(urlTimeline)

        navegador_web.obterElementoAposCarregamento(CSS_VERIFICAR_CARREGAMENTO)
        htmlTimeline = navegador_web.obterElementoAposCarregamento(CSS_DADOS_TIMELINE)

        hashUrlPartida = hash_utils.gerar_hash(urlPartida)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        metadados = {
            "url_partida": urlPartida,
            "hash_url_partida": hashUrlPartida,
            "tipo_extracao": "EXT_HTML_PARTIDA_TIMELINE"
        }

        htmlFinal = html_utils.incluirMetadadosHtml(str(htmlTimeline), metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"{uuid.lower()}.html",
                "tipo": "PROC_HTML_PARTIDA_TIMELINE",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlPartida": urlPartida,
                           "hashUrlPartida": hashUrlPartida,
                           }
            })

    except Exception as e:
        log.ERRO("Não foi possível obter timeline de eventos da partida.", e.args)
        return None
    finally:
        navegador_web.finalizarNavegadorWeb()


def extrairHtmlEstatisticasPartida(urlPartida: str):
    try:
        CSS_DADOS_ESTATISTICAS = "body"
        CSS_VERIFICAR_CARREGAMENTO = "div[class^=statRow]"

        urlEstatisticas = f"{urlPartida}#/resumo-de-jogo/estatisticas-de-jogo/"
        navegador_web.navegar(urlEstatisticas)

        navegador_web.obterElementoAposCarregamento(CSS_VERIFICAR_CARREGAMENTO)
        htmlEstatisticas = navegador_web.obterElementoAposCarregamento(CSS_DADOS_ESTATISTICAS)

        hashUrlPartida = hash_utils.gerar_hash(urlPartida)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        metadados = {
            "url_partida": urlPartida,
            "hash_url_partida": hashUrlPartida,
            "tipo_extracao": "EXT_HTML_PARTIDA_ESTATISTICAS"
        }

        htmlFinal = html_utils.incluirMetadadosHtml(htmlEstatisticas.get_attribute("outerHTML"), metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"ptd-stat-{hashUrlPartida.lower()}.html",
                "tipo": "PROC_HTML_PARTIDA_ESTATISTICAS",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "urlPartida": urlPartida,
                           "hasUrlPartida": hashUrlPartida}
            })


    except Exception as e:
        log.ERRO("Não foi possível extrair HTML estatísticas da partida.", e.args)
        return None


def extrairHtmlUltimasPartidasEquipes(urlPartida: str):
    try:
        DADOS_H2H = "body"
        CSS_H2H_EXIBIR_MAIS = ".h2h__showMore"
        CSS_VERIFICAR_CARREGAMENTO = "div[class^=h2h]"

        urlHeadToHead = f"{urlPartida}#/h2h/"
        navegador_web.navegar(urlHeadToHead)
        navegador_web.obterElementoAposCarregamento(CSS_VERIFICAR_CARREGAMENTO)

        browser = navegador_web.obterNavegadorWeb()
        botoesExibirMais = browser.find_elements(By.CSS_SELECTOR, CSS_H2H_EXIBIR_MAIS)

        for botao in botoesExibirMais:
            for i in range(0, 6):
                botao.click()

        htmlHeadToHead = navegador_web.obterElementoAposCarregamento(DADOS_H2H)

        hashUrlPartida = hash_utils.gerar_hash(urlPartida)
        id = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        metadados = {
            "url_partida": urlPartida,
            "hash_url_partida": hashUrlPartida,
            "tipo_extracao": "EXT_HTML_PARTIDA_H2H"
        }
        htmlFinal = html_utils.incluirMetadadosHtml(htmlHeadToHead.get_attribute("outerHTML"), metadados)

        return ItemProcessamento(
            **{
                "id": id,
                "urlPartida": urlPartida,
                "hashUrlPartida": hashUrlPartida,
                "tipo": "PROC_HTML_PARTIDA_H2H",
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "nomeArquivo": f"ptd-h2h-{hashUrlPartida.lower()}.html"}
            })

    except Exception as e:
        log.ERRO("Não foi possível extrair HTML últimas partidas das equipes.", e.args)
        return None


def extrairHtmlOddsPartida(urlPartida: str, mercado: MERCADO):
    try:
        urlOdds = f"{urlPartida}#/comparacao-de-odds/"
        tipoExtracao = f"EXT_HTML_PARTIDA_ODDS_{mercado.name}"
        prefixoArquivo = "ptd-odd-rst"

        if mercado == MERCADO.DNB:
            urlOdds = f"{urlPartida}#/comparacao-de-odds/home-away/"
            prefixoArquivo = "ptd-odd-dnb"

        if mercado == MERCADO.DUPLA_CHANCE:
            urlOdds = f"{urlPartida}#/comparacao-de-odds/double-chance/"
            prefixoArquivo = "ptd-odd-dc"

        if mercado == MERCADO.IMPAR_PAR:
            urlOdds = f"{urlPartida}#/comparacao-de-odds/odd-even/"
            prefixoArquivo = "ptd-odd-ip"

        if mercado == MERCADO.AMBOS_MARCAM:
            urlOdds = f"{urlPartida}#/comparacao-de-odds/ambos-marcam/"
            prefixoArquivo = "ptd-odd-btts"

        if mercado == MERCADO.PLACAR_EXATO:
            urlOdds = f"{urlPartida}#/comparacao-de-odds/correct-score/"
            prefixoArquivo = "ptd-odd-plc"

        if mercado == MERCADO.UNDER_OVER:
            urlOdds = f"{urlPartida}#/comparacao-de-odds/acima-abaixo/"
            prefixoArquivo = "ptd-odd-uo"

        return extrairHtmlOdds(urlOdds, tipoExtracao, prefixoArquivo)
    except Exception as e:
        log.ERRO(f"Não foi possível extrair HTML odds de {mercado.name} da partida.", e.args)
        return None


def extrairHtmlOdds(urlOdds: str, tipoExtracao: str, prefixoArquivo: str):
    try:
        CSS_DADOS_ODDS = "body"
        CSS_VERIFICAR_CARREGAMENTO = "#detail > div > div.subTabs"

        navegador_web.navegar(urlOdds)

        navegador_web.obterElementoAposCarregamento(CSS_VERIFICAR_CARREGAMENTO)
        htmlOdds = navegador_web.obterElementoAposCarregamento(CSS_DADOS_ODDS)

        urlHash = hash_utils.gerar_hash(urlOdds)
        uuid = auth_service.gerarIdentificadorUniversal()
        dataHoraExtracao = datetime.now()

        metadados = {
            "url_odds": urlOdds,
            "hash_url_odds": urlHash,
            "tipo_extracao": tipoExtracao
        }

        htmlFinal = html_utils.incluirMetadadosHtml(htmlOdds.get_attribute("outerHTML"), metadados)

        return ItemProcessamento(
            **{
                "uuid": uuid,
                "nomeArquivo": f"{prefixoArquivo}-{urlHash.lower()}.html",
                "tipo": tipoExtracao,
                "dataHora": dataHoraExtracao,
                "extras": {"html": string_utils.limparString(str(htmlFinal)),
                           "url_odds": urlOdds,
                           "hash_url_odds": urlHash
                           }
            })

    except Exception as e:
        log.ERRO(f"Não foi possível extrair HTML odds da partida. [{urlOdds}][{tipoExtracao}]", e.args)
        return None
