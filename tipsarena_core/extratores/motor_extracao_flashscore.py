# -*- coding: utf-8 -*-
import os

from tipsarena_core import gerenciador_filas
from tipsarena_core.enums.enum_aposta import MERCADO
from tipsarena_core.enums.enum_fila import FILA
from tipsarena_core.extratores.flash_score import (extrator_competicao,
                                                   extrator_edicao_competicao,
                                                   extrator_equipe,
                                                   extrator_pais,
                                                   extrator_partida)
from tipsarena_core.models.item_extracao import ItemExtracao


def extrairPartidasDia(dia=0):
    dadosExtracao = extrator_partida.extrairHtmlPartidasDia(dia)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]

    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/partidas_dia/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_PARTIDAS_DIA.value,
                                       dadosExtracao.__dict__)


def extrairHtmlPartida(itemExtracao: ItemExtracao):
    urlPartida = itemExtracao.url
    itemProcessamento = extrator_partida.extrairHtmlPartida(urlPartida, itemExtracao.extras)
    nomeArquivo = itemProcessamento.nomeArquivo
    html = itemProcessamento.extras["html"]

    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/partida/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del itemProcessamento.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_PARTIDA.value,
                                       itemProcessamento.__dict__)


def extrairHtmlPaises():
    try:
        fila = FILA.FL_PROC_HTML_LISTA_PAISES.value
        itemProcessamento = extrator_pais.extrairHtmlPaises()
        html = itemProcessamento.extras["html"]
        caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}paises/{itemProcessamento.nomeArquivo}"
        with open(caminhoArquivo, mode="w") as arquivo:
            arquivo.write(html)

        del itemProcessamento.extras["html"]

        gerenciador_filas.produzirMensagem(fila, itemProcessamento.__dict__)
    except Exception as e:
        gerenciador_filas.produzirMensagem(f'erro-{fila}',
                                           {'payload': itemProcessamento.__dict__,
                                            'erro': f'{e.__str__()}'}
                                           )


def extrairHtmlCompeticoesPais(urlPais: str, metadados={}):
    dadosExtracao = extrator_competicao.extrairHtmlCompeticoesPais(urlPais, metadados)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}competicoes/{nomeArquivo}"

    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_COMPETICOES_PAIS.value,
                                       dadosExtracao.__dict__)


def extrairHtmlEdicoesCompeticao(urlCompeticao: str, metadados={}):
    dadosExtracao = extrator_competicao.extrairHtmlEdicoesCompeticao(
        urlCompeticao, metadados)

    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}edicoes_competicao/{nomeArquivo}"

    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_EDICOES_COMPETICAO.value,
                                       dadosExtracao.__dict__)


def extrairHtmlEdicaoCompeticao(urlEdicao: str, metadados: dict):
    dadosExtracao = extrator_edicao_competicao.extrairHtmlEdicaoCompeticao(urlEdicao, metadados)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}edicoes_competicao/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_EDICAO_COMPETICAO.value,
                                       dadosExtracao.__dict__)


def extrairHtmlPartidasEdicaoCompeticao(urlEdicao: str, metadados: dict):
    itemProcessamento = extrator_edicao_competicao. \
        extrairHtmlPartidasFinalizadasEdicaoCompeticao(urlEdicao, metadados)

    html = itemProcessamento.extras["html"]
    caminhoArquivoPartidasFinalizadas = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/partidas_edicao/" \
                                        f"{itemProcessamento.nomeArquivo}"

    with open(caminhoArquivoPartidasFinalizadas, "w") as arquivo:
        arquivo.write(html)

    del itemProcessamento.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_PARTIDAS_EDICAO_COMPETICAO.value,
                                       itemProcessamento.__dict__)

    if metadados["edicao_em_andamento"]:
        itemProcessamento = extrator_edicao_competicao.extrairHtmlPartidasAgendadasEdicaoCompeticao(urlEdicao,
                                                                                                    metadados)
        html = itemProcessamento.extras["html"]
        caminhoArquivoPartidasAgendadas = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/partidas_edicao/" \
                                          f"{itemProcessamento.nomeArquivo}"

        with open(caminhoArquivoPartidasAgendadas, "w") as arquivo:
            arquivo.write(html)

        del itemProcessamento.extras["html"]
        gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_PARTIDAS_EDICAO_COMPETICAO.value,
                                           itemProcessamento.__dict__)


def extrairHtmlEquipesEdicaoCompeticao(urlEdicao: str, metadados: dict):
    itemProcessamento = extrator_equipe.extrairHtmlEquipesEdicaoCompeticao(urlEdicao, metadados)
    nomeArquivo = itemProcessamento.nomeArquivo
    html = itemProcessamento.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}equipes/equipes_edicao/{nomeArquivo}"

    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del itemProcessamento.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_EQUIPES_EDICAO_COMPETICAO.value,
                                       itemProcessamento.__dict__)


def extrairHtmlEstatisticasPartida(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlEstatisticasPartida(urlPartida)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/estatisticas/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ESTATISTICAS_PARTIDA.value,
                                       dadosExtracao.__dict__)


def extrairHtmlClassificacao(itemExtracao: ItemExtracao):
    urlPartida = itemExtracao.url
    itemProcessamento = extrator_partida.extrairHtmlClassificacao(urlPartida, itemExtracao.extras)
    html = itemProcessamento.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/classificacao/{itemProcessamento.nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del itemProcessamento.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_CLASSIFICACAO_PARTIDA.value,
                                       itemProcessamento.__dict__)


def extrairHtmlConfrontosEquipes(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlUltimasPartidasEquipes(urlPartida)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/confrontos/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_H2H_PARTIDA.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesResultado(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.RESULTADO)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_RESULTADO.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesDNB(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.DNB)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_DNB.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesDuplaChance(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.DUPLA_CHANCE)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_DUPLA_CHANCE.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesImparPar(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.IMPAR_PAR)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_IMPAR_PAR.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesAmbosMarcam(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.AMBOS_MARCAM)
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{dadosExtracao.nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_AMBOS_MARCAM.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesPlacarExato(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.PLACAR_EXATO)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_PLACAR_EXATO.value,
                                       dadosExtracao.__dict__)


def extrairHtmlCotacoesUnderOver(urlPartida: str):
    dadosExtracao = extrator_partida.extrairHtmlOddsPartida(urlPartida, MERCADO.UNDER_OVER)
    nomeArquivo = dadosExtracao.nomeArquivo
    html = dadosExtracao.extras["html"]
    caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/odds/{nomeArquivo}"
    with open(caminhoArquivo, "w") as arquivo:
        arquivo.write(html)

    del dadosExtracao.extras["html"]

    gerenciador_filas.produzirMensagem(FILA.FL_PROC_HTML_ODDS_UNDER_OVER.value,
                                       dadosExtracao.__dict__)


if __name__ == "__main__":
    # extrairPartidasDia(-3)

    # extrairHtmlPaises()
    # extrairHtmlCompeticoesPais("https://www.flashscore.com.br/futebol/brasil/", {"nome_pais": "Brasil"})
    # extrairHtmlEdicoesCompeticao("https://www.flashscore.com.br/futebol/africa-do-sul/primeira-liga/",
    #                              {"nome_pais": "África do Sul",
    #                               "url_pais": "https://www.flashscore.com.br/futebol/africa-do-sul/",
    #                               "hash_url_pais": "1cec8d62ed579e41cadf56ff"})
    #
    extrairHtmlPartidasEdicaoCompeticao("https://www.flashscore.com.br/futebol/inglaterra/campeonato-ingles-2022-2023/",
                                        {"edicao_em_andamento": True})
    # extrairHtmlEquipesEdicaoCompeticao(
    #   {"url": "https://www.flashscore.com.br/futebol/inglaterra/campeonato-ingles-2019-2020/",
    #    "emAndamento": False}
    # )
    # extrairHtmlPartida({"urlPartida": "https://www.flashscore.com.br/jogo/AwCj20Vo/","teste":"metadadossssssss"})
    #
    # extrairHtmlEstatisticasPartida("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlConfrontosEquipes("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlClassificacao("https://www.flashscore.com.br/jogo/ChVVIS0m/")
    #
    # extrairHtmlCotacoesResultado("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlCotacoesDNB("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlCotacoesDuplaChance("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlCotacoesImparPar("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlCotacoesAmbosMarcam("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlCotacoesPlacarExato("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlCotacoesUnderOver("https://www.flashscore.com.br/jogo/AwCj20Vo/")
    # extrairHtmlClassificacao(
    #   ItemExtracao(
    #     **{"url": "https://www.flashscore.com.br/jogo/IBLYMk1B/",
    #        "hashUrl": "teste",
    #        "uuid": "testet djalskfa f",
    #        "tipo": "teste"
    #        })
    # )
    pass
