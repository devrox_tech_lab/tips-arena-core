# -*- coding: utf-8 -*-
import json
import os

import pika

from tipsarena_core.enums.enum_fila import FILA
from tipsarena_core.models import serealizador_json
from tipsarena_core.services import log_service as log

conn = pika.BlockingConnection(pika.ConnectionParameters(host=os.getenv("TA_RABBITMQ_URI")))
channel = conn.channel()


def obterFilaPorChave(chave: str):
    for fila in FILA:
        if fila.name == chave:
            return fila


def produzirMensagem(fila: str, payload: dict):
    try:
        body = json.dumps(payload, default=serealizador_json)
        channel.queue_declare(queue=fila)
        channel.basic_publish(exchange="",
                              routing_key=fila,
                              body=body)
        log.INFO(f"Mensagem incluída na fila: {fila}")
    except Exception as e:
        log.ERRO(f"Erro ao produzir mensagem para a fila '{fila}'. [{payload}]", e.args)


def consumirMensagens(fila: str, callback):
    try:
        channel.queue_declare(queue=fila)
        channel.basic_consume(queue=fila, on_message_callback=callback, auto_ack=True)
        log.INFO(f"Aguardando mensagens [{fila}]")
        channel.start_consuming()
    except Exception as e:
        log.ERRO(f"Erro ao consumir mensagens da fila {fila}...", e)


def callbackDefault(ch, method, properties, body):
    try:
        log.OK(f"Mensagem recebida: {body}")
    except Exception as e:
        log.ERRO(f"Erro ao produzir mensagem: [{e.args}].")


if __name__ == '__main__':
    # deletarTopicos()
    # criarTopicos()
    produzirMensagem("ta-fila-teste", {"mensgem": "Teste OK!"})
