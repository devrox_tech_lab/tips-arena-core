# -*- coding: utf-8 -*-
import json
import os

from tipsarena_core import gerenciador_filas
from tipsarena_core.enums.enum_fila import FILA
from tipsarena_core.models.item_processamento import ItemProcessamento
from tipsarena_core.parsers_html import motor_parser_flashscore
from tipsarena_core.services import log_service as log

fila = FILA.FL_PROC_HTML_COMPETICOES_PAIS.value


def iniciar():
    def processarMensagem(ch, method, properties, body):
        """
        Método para consumir processar as mensagens presentes no tópico de processamento do html das competições de um país..
        """
        try:
            dadosMensagem = json.loads(body)
            itemProcessamento = ItemProcessamento(**dadosMensagem)

            caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}competicoes/{itemProcessamento.nomeArquivo}"
            motor_parser_flashscore.processarHtmlCompeticoesPais(caminhoArquivo)

        except Exception as e:
            log.ERRO(f"Erro ao processar mensagem da fila: {fila}", e.args)
            gerenciador_filas.produzirMensagem(f'erro-{fila}',
                                               {'payload': dadosMensagem,
                                                'erro': f'{e.args}'})

    gerenciador_filas.consumirMensagens(fila, processarMensagem)


if __name__ == "__main__":
    try:
        iniciar()
    except KeyboardInterrupt:
        log.INFO("Processo interrompido.")
