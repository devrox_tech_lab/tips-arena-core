# -*- coding: utf-8 -*-
import json
import os

from tipsarena_core import gerenciador_filas
from tipsarena_core.enums.enum_fila import FILA
from tipsarena_core.models.item_extracao import ItemExtracao
from tipsarena_core.parsers_html import motor_parser_flashscore
from tipsarena_core.services import log_service as log

fila = FILA.FL_EXT_HTML_ESTATISTICAS_PARTIDA.value


def main():
    def processarMensagem(ch, method, properties, body):
        try:
            dadosMensagem = json.loads(body)
            itemProcessamento = ItemExtracao(**dadosMensagem)
            nomeArquivo = itemProcessamento.extras["nomeArquivo"]
            caminhoArquivo = f"{os.getenv('TA_DIR_ARQUIVOS_PARA_PROCESSAR')}partidas/estatisticas/{nomeArquivo}"
            motor_parser_flashscore.processarHtmlEstatisticasPartida(caminhoArquivo)

        except Exception as e:
            log.ERRO(f"Erro ao processar mensagem da fila: {fila}", e.args)

    gerenciador_filas(fila, processarMensagem)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.INFO("Processo interrompido!")
