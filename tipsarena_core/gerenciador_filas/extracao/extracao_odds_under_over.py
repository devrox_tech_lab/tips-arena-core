# -*- coding: utf-8 -*-
import json

from tipsarena_core import gerenciador_filas
from tipsarena_core.enums.enum_fila import FILA
from tipsarena_core.extratores import motor_extracao_flashscore
from tipsarena_core.models.item_extracao import ItemExtracao
from tipsarena_core.services import log_service as log

fila = FILA.FL_EXT_HTML_ODDS_UNDER_OVER.value


def main():
    def processarMensagem(ch, method, properties, body):
        """
        Método para consumir processar as mensagens presentes na fila de extração do html
        das odds Under/Over de uma partida.
        """
        try:
            dadosMensagem = json.loads(body)
            itemProcessamento = ItemExtracao(dadosMensagem)
            motor_extracao_flashscore.extrairHtmlCotacoesUnderOver(itemProcessamento.url)

        except Exception as e:
            log.ERRO(f"Erro ao processar mensagem da fila: {fila}", e.args)

        gerenciador_filas.consumirMensagens(fila, processarMensagem)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.INFO("Processo interrompido!")
