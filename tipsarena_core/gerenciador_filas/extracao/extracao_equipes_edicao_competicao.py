# -*- coding: utf-8 -*-
import json

from tipsarena_core import gerenciador_filas
from tipsarena_core.enums.enum_fila import FILA
from tipsarena_core.extratores import motor_extracao_flashscore
from tipsarena_core.models.item_extracao import ItemExtracao
from tipsarena_core.services import log_service as log

fila = FILA.FL_PROC_HTML_EQUIPES_EDICAO_COMPETICAO.value


def main():
    def processarMensagem(mensagem):
        """
        Método para consumir processar as mensagens presentes
        no tópico de extração do html das equipes de uma edição de competição.
        """
        try:
            payload = mensagem.value().decode("UTF-8")
            dadosMensagem = json.loads(payload)
            itemProcessamento = ItemExtracao(dadosMensagem)
            motor_extracao_flashscore.extrairHtmlEquipesEdicaoCompeticao(itemProcessamento.url,
                                                                         itemProcessamento.extras)

        except Exception as e:
            log.ERRO(f"Erro ao processar mensagem da fila: {fila}", e.args)

        gerenciador_filas.consumirMensagens(fila, processarMensagem)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.INFO("Processo interrompido!")
