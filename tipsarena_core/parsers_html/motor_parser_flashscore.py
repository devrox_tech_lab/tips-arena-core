# -*- coding: utf-8 -*-
import shutil

from tipsarena_core import gerenciador_filas
from tipsarena_core.core import (competicao_core, edicao_competicao_core,
                                 pais_core, partida_core, probabilidade_core)
from tipsarena_core.enums.enum_fila import FILA as FILA
from tipsarena_core.models.item_extracao import ItemExtracao
from tipsarena_core.parsers_html.flash_score import (
    parser_competicao, parser_edicao_competicao, parser_equipe, parser_pais,
    parser_partida, parser_partida_classificacao, parser_partida_estatisticas,
    parser_partida_odds, parser_partida_timeline)
from tipsarena_core.services import auth_service
from tipsarena_core.utils import hash_utils, html_utils


def processarHtmlPaises(caminhoParaArquivo: str):
    try:
        fila = FILA.FL_EXT_HTML_COMPETICOES_PAIS.value
        with open(caminhoParaArquivo, "r") as arquivo:
            listaPaises = parser_pais.processarHtmlListaPaises(arquivo)

            for pais, itemExtracao in listaPaises:
                resultado = pais_core.salvarPais(pais)
                gerenciador_filas.produzirMensagem(fila, itemExtracao.__dict__)
    except Exception as e:
        gerenciador_filas.produzirMensagem(f'erro-{fila}',
                                           {'payload': {'arquivo': caminhoParaArquivo},
                                            'erro': f'{e.args}'}
                                           )


def processarHtmlCompeticoesPais(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        listaCompeticoes = parser_competicao.processarHtmlCompeticoesPais(html)

        for pais, competicao, itemExtracao in listaCompeticoes:
            resultado = competicao_core.salvarCompeticao(competicao)
            gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_EDICOES_COMPETICAO.value, itemExtracao.__dict__)


def processarHtmlEdicoesCompeticao(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()

    listaEdicoes = parser_competicao.processarHtmlEdicoesCompeticao(html)

    for pais, competicao, edicao in listaEdicoes:
        resultado = [pais_core.salvarPais(pais),
                     competicao_core.salvarCompeticao(competicao),
                     edicao_competicao_core.salvarEdicaoCompeticao(edicao)]

        itemExtracao = ItemExtracao(
            **{"uuid": auth_service.gerarIdentificadorUniversal(),
               "url": edicao.url,
               "hashUrl": edicao.id,
               "tipo": "EXT_HTML_EDICAO_COMPETICAO",
               "extras": edicao.__dict__})

        itemExtracao.tipo = "EXT_HTML_PARTIDAS_EDICAO_COMPETEICAO"
        gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_PARTIDAS_EDICAO_COMPETICAO.value, itemExtracao.__dict__)

        itemExtracao.tipo = "EXT_HTML_EQUIPES_EDICAO_COMPETICAO"
        gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_EQUIPES_EDICAO_COMPETICAO.value, itemExtracao.__dict__)


def processarHtmlEdicaoCompeticao(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()

    edicaoCompeticao = parser_edicao_competicao.processarHtmlEdicaoCompeticao(html)
    pass


def processarHtmlPartidasEdicaoCompeticao(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()

    partidas = parser_partida.processarHtmlPartidasEdicaoCompeticao(html)

    for partida in partidas:
        gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_PARTIDA.value, partida)


def processarHtmlPartidasDia(caminhoParaArquivo: str):
    try:
        with open(caminhoParaArquivo, "r") as arquivo:
            html = arquivo.read()

        partidas = parser_partida.processarHtmlListaDePartidas(html)

        for partida in partidas:
            itemExtracao = ItemExtracao(
                **{
                    "uuid": auth_service.gerarIdentificadorUniversal(),
                    "url": partida["url_partida"],
                    "hashUrl": hash_utils.gerar_hash(partida["url_partida"]),
                    "tipo": "EXT_HTML_PARTIDA",
                    "extras": partida
                })

            gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_PARTIDA.value, itemExtracao.__dict__)

        shutil.move(caminhoParaArquivo, caminhoParaArquivo.replace("para_processar", "processado"))
    except Exception as e:
        shutil.move(caminhoParaArquivo, caminhoParaArquivo.replace("para_processar", "erro"))
        raise e


def processarHtmlEquipesEdicaoCompeticao(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        metadadosEdicao = html_utils.obterMetadosHtml(html)
        equipes = parser_equipe.processarHtmlEquipesCompeticao(html)
        for equipe in equipes:
            equipe["edicaoCompeticao"] = metadadosEdicao
            gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_EQUIPE.value, equipe)


def processarHtmlPartida(caminhoParaArquivo: str):
    try:
        with open(caminhoParaArquivo, "r") as arquivo:
            html = arquivo.read()

        partida = parser_partida.processarHtmlPartida(html)
        resultado = partida_core.salvarPartida(partida)

        informacoesDisponiveis = partida.informacoesDisponiveis

        if informacoesDisponiveis["classificacao"]:
            itemExtracao = ItemExtracao(
                **{
                    "uuid": auth_service.gerarIdentificadorUniversal(),
                    "url": partida.urlPartida,
                    "hashUrl": hash_utils.gerar_hash(partida.urlPartida),
                    "tipo": "EXT_HTML_CLASSIFICACAO_PARTIDA",
                    "extras": partida.__dict__
                })
            gerenciador_filas.produzirMensagem(FILA.FL_EXT_HTML_CLASSIFICACAO_PARTIDA.value, itemExtracao.__dict__)

        shutil.move(caminhoParaArquivo, caminhoParaArquivo.replace("para_processar", "processado"))
    except Exception as e:
        shutil.move(caminhoParaArquivo, caminhoParaArquivo.replace("para_processar", "erro"))
        raise e


def processarHtmlClassificacao(caminhoParaArquivo: str):
    try:
        with open(caminhoParaArquivo, "r") as arquivo:
            html = arquivo.read()

        metadados = html_utils.converterTextoParaHtml(html).select("metadados dados")
        for dados in metadados:
            for key in dados.attrs:
                if key == "url_equipe_mandante":
                    urlEquipeMandante = dados["url_equipe_mandante"]
                if key == "url_equipe_visitante":
                    urlEquipeVisitante = dados["url_equipe_visitante"]
                if key == "hash_url_partida":
                    idPartida = dados["hash_url_partida"]

        classificacoes = parser_partida_classificacao.processarHtmlClassificacoes(html)
        expectativasDeGol = probabilidade_core.calcularExpectativaDeGols(urlEquipeMandante,
                                                                         urlEquipeVisitante,
                                                                         classificacoes['classificacaoGeral'],
                                                                         classificacoes['classificacaoCasa'],
                                                                         classificacoes['classificacaoFora'])

        expectativasDeGol.idPartida = idPartida
        expectativasDeGol.idEquipeMandante = hash_utils.gerar_hash(urlEquipeMandante)
        expectativasDeGol.idEquipeVisitante = hash_utils.gerar_hash(urlEquipeVisitante)

        resultado = probabilidade_core.salvarExpectativasDeGol(expectativasDeGol)
        shutil.move(caminhoParaArquivo, caminhoParaArquivo.replace("para_processar", "processado"))
    except Exception as e:
        shutil.move(caminhoParaArquivo, caminhoParaArquivo.replace("para_processar", "erro"))
        raise e


def processarHtmlTimelinePartida(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        timeline = parser_partida_timeline.processarHtmlTimeline(html)
        for evento in timeline:
            pass


def processarHtmlEstatisticasPartida(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        estatisticas = parser_partida_estatisticas.processarHtmlEstatisticas(html)
        for estatistica in estatisticas:
            pass


def processarHtmlCotacoesResultado(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsResultado(html)
        for cotacao in cotacoes:
            pass


def processarHtmlCotacoesDNB(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsDrawNoBet(html)
        pass


def processarHtmlCotacoesDuplaChance(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsDuplaChance(html)
        pass


def processarHtmlCotacoesImparPar(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsImparPar(html)
        pass


def processarHtmlCotacoesAmbosMarcam(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsBtts(html)
        pass


def processarHtmlCotacoesPlacarExato(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsPlacarExato(html)
        for cotacao in cotacoes:
            pass


def processarHtmlCotacoesUnderOver(caminhoParaArquivo: str):
    with open(caminhoParaArquivo, "r") as arquivo:
        html = arquivo.read()
        cotacoes = parser_partida_odds.processarHtmlOddsUnderOver(html)
        for cotacao in cotacoes:
            pass


if __name__ == "__main__":
    # processarHtmlPaises(
    #     '/Volumes/HD/Documents/tips_arena/tests/arquivos/para_processar/paises/ps-b3a8ebf43551bf390ad6733f.html')
    # processarHtmlCompeticoesPais(
    #   "/Volumes/HD/Documents/tips_arena/tests/arquivos/para_processar/competicoes/cmp-ps-e5716470e16fc3fd951ceaaf.html")
    # processarHtmlEdicoesCompeticao(
    #   '/Volumes/HD/Documents/tips_arena/tests/arquivos/para_processar/edicoes_competicao/edc-cmp-9a250a9d4f1df7ef4d34704e.html')
    # extrairHtmlEdicaoCompeticao("https://www.flashscore.com.br/futebol/inglaterra/campeonato-ingles-2019-2020/")
    # processarHtmlEdicaoCompeticao(
    #   '/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/edicao_competicao/edc-cmp-d050e62f21c2b26c7277582b.html')

    # processarHtmlPartidasEdicaoCompeticao(
    #     '/Volumes/HD/Documents/tips_arena/tests/arquivos/para_processar/partidas/partidas_edicao/'
    #     'ptd-edc-bc01bb01fbbc3ac6d50d4c8a.html'
    # )

    processarHtmlPartidasDia(
        '/Volumes/HD/Documents/tips_arena/tests/arquivos/para_processar/partidas/partidas_edicao/'
        'ptd-edc-bc01bb01fbbc3ac6d50d4c8a.html')

    # processarHtmlPartida(
    #   '/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partidas/partida/ptd-09ae7cbc7754c616ff4da36f.html')
    # # extrairHtmlEquipesEdicaoCompeticao("https://www.flashscore.com.br/futebol/inglaterra/campeonato-ingles-2019-2020/")

    # processarHtmlEquipesEdicaoCompeticao('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/equipe/6105e77d-7c00-4b66-a6c2-b864d78ba510.html')

    # processarHtmlEstatisticasPartida('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/estatisticas/09ae7cbc7754c616ff4da36f.html')
    # processarHtmlTimelinePartida(
    #   '/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/09ae7cbc7754c616ff4da36f.html')
    # processarHtmlCotacoesResultado(
    #   '/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/09ae7cbc7754c616ff4da36f.html')
    # processarHtmlCotacoesDNB('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/ab3567ca270bbefbd60dcf1e.html')
    # processarHtmlCotacoesDuplaChance('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/30b2c165536df3d3335fb4f3.html')
    # processarHtmlCotacoesImparPar('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/b6460593435c695a7302a892.html')
    # processarHtmlCotacoesAmbosMarcam('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/4edbd6bc5682ecd5ae1725c1.html')
    # processarHtmlCotacoesPlacarExato('/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/78c09da5c2fbe222a8ae6bde.html')
    # processarHtmlCotacoesUnderOver(
    #   '/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partida/odd/4aa57f80a1e0a68e08f3f154.html')
    # processarHtmlClassificacao(
    #   '/Volumes/HD/Documents/tips_arena/tipsarena_tests/arquivos/para_processar/partidas/classificacao/ptd-cls-87275e2e0a9c8eaf5dc2a78b.html')
    pass
