# -*- coding: utf-8 -*-

from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.item_extracao import ItemExtracao
from tipsarena_core.models.pais import Pais
from tipsarena_core.services import auth_service
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import hash_utils, html_utils


def processarHtmlListaPaises(html: str):
    try:
        CSS_ITEM_PAIS = "a[id^=lmenu_]"
        documentoHtml = html_utils.converterTextoParaHtml(html)
        htmlPaises = documentoHtml.select(CSS_ITEM_PAIS)

        for pais in htmlPaises:
            url = pais.attrs["href"]

            if url != "#":
                urlPais = f"{navegador_web.URL_BASE}{url}"
                nomePais = pais.text.strip()

                pais = Pais(
                    **{
                        "id": hash_utils.gerar_hash(urlPais),
                        "nome": nomePais,
                        "url": urlPais,
                        "urlBandeira": ""
                    }
                )

                yield (pais,
                       ItemExtracao(
                           **{
                               "uuid": auth_service.gerarIdentificadorUniversal(),
                               "url": urlPais,
                               "hashUrl": hash_utils.gerar_hash(urlPais),
                               "tipo": "EXT_HTML_COMPETICOES_PAIS",
                               "extras": {
                                   "nome_pais": nomePais,
                                   "url_pais": urlPais,
                                   "hash_url_pais": hash_utils.gerar_hash(urlPais)
                               }
                           })
                       )


    except Exception as e:
        log.ERRO("Não foi possível processar html lista de países.", e.args)
        return None
