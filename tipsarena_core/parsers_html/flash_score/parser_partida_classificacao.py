from tipsarena_core.models.classificacao_competicao import Classificacao
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import html_utils


def processarHtmlClassificacoes(html: str):
    try:
        CSS_TABELA_CLASSIFICACAO_GERAL = "#classificacao_geral"
        CSS_TABELA_CLASSIFICACAO_CASA = "#classificacao_casa"
        CSS_TABELA_CLASSIFICACAO_FORA = "#classificacao_fora"

        htmlClassificacao = html_utils.converterTextoParaHtml(html)

        htmlClassificacaoGeral = htmlClassificacao.select_one(CSS_TABELA_CLASSIFICACAO_GERAL)
        htmlClassificacaoCasa = htmlClassificacao.select_one(CSS_TABELA_CLASSIFICACAO_CASA)
        htmlClassificacaoFora = htmlClassificacao.select_one(CSS_TABELA_CLASSIFICACAO_FORA)

        classificacaoGeral = processarHtmlTabelaClassificacao(htmlClassificacaoGeral)
        classificacaoCasa = processarHtmlTabelaClassificacao(htmlClassificacaoCasa)
        classificacaoFora = processarHtmlTabelaClassificacao(htmlClassificacaoFora)

        return {
            "classificacaoGeral": classificacaoGeral,
            "classificacaoCasa": classificacaoCasa,
            "classificacaoFora": classificacaoFora
        }
    except Exception as e:
        log.ERRO("Não foi possível obter últimas partidas das equipes.", e.args)
        return None


def processarHtmlTabelaClassificacao(htmlClassificacao):
    try:
        CSS_LINHAS_CLASSIFICACAO = ".ui-table__row"
        CSS_POSICAO = ".table__cell--rank"
        CSS_EQUIPE = "a.tableCellParticipant__name"
        CSS_VALORES = "span.table__cell.table__cell--value"
        CSS_FORMA = "div.table__cell--form>div"

        tabela = []
        htmlListaClassificacao = htmlClassificacao.select(CSS_LINHAS_CLASSIFICACAO)

        for html in htmlListaClassificacao:
            posicao = html.select_one(CSS_POSICAO).text
            htmlEquipe = html.select_one(CSS_EQUIPE)
            nomeEquipe = htmlEquipe.text
            urlEquipe = f"{htmlEquipe.attrs['href']}"
            htmlValores = html.select(CSS_VALORES)
            valores = [span.text for span in htmlValores]
            htmlForma = html.select(CSS_FORMA)
            forma = [div.text for div in htmlForma]

            tabela.append(
                Classificacao(
                    **{"posicao": int(posicao.replace(".", "")),
                       "urlEquipe": urlEquipe,
                       "nomeEquipe": nomeEquipe,
                       "jogos": int(valores[0]),
                       "vitorias": int(valores[1]),
                       "empates": int(valores[2]),
                       "derrotas": int(valores[3]),
                       "golsMarcados": int(valores[4].split(":")[0]),
                       "golsSofridos": int(valores[4].split(":")[1]),
                       "pontos": int(valores[5]),
                       "forma": forma
                       }))

        return tabela
    except Exception as e:
        log.ERRO("Não foi possível processar tabela de partidas da equipe.", e.args)
        return []
