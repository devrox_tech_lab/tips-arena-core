# -*- coding: utf-8 -*-

from tipsarena_core.core import edicao_competicao_core
from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.competicao import Competicao
from tipsarena_core.models.edicao_competicao import EdicaoCompeticao
from tipsarena_core.models.item_extracao import ItemExtracao
from tipsarena_core.models.pais import Pais
from tipsarena_core.services import auth_service
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import hash_utils, html_utils, string_utils


def processarHtmlCompeticao(html: str):
    try:
        documentoHtml = html_utils.converterTextoParaHtml(html)
        linksCabecalho = html_utils.obterDadosCabecalho(documentoHtml)

        paisCompeticao = {"url": linksCabecalho[1]["href"],
                          "nome": linksCabecalho[1]["text"]}

        anoEdicao = documentoHtml.select(".teamHeader__text")[0].text
        # urlCompeticao = urlCompeticao[:-1] + "-" + anoEdicao.replace("/", "-") + "/"

        nomeCompeticao = documentoHtml.select(".teamHeader__name")[0].text
        logoCompeticao = documentoHtml.select(".teamHeader__logo")
        logoCompeticao = logoCompeticao[0]["style"].split("(")[1]
        logoCompeticao = logoCompeticao.replace(")", "")
        logoCompeticao = logoCompeticao.replace("\\", "")
        logoCompeticao = logoCompeticao.replace("'", "")

        return {
            "nome": string_utils.limparString(nomeCompeticao),
            "pais": paisCompeticao,
            "urlLogo": logoCompeticao,
            "url": "",
            "anoEdicao": anoEdicao,
            "totalPartidas": 0,
            "totalPartidasFinalizadas": 0,
            "status": "NAO_DEFINIDO",
            "equipeCampea": None
        }

    except Exception as e:
        log.ERRO("Erro ao extrair dados da competição [{}]", e.args)
        return None


def processarHtmlCompeticoesPais(html: str):
    try:
        CSS_LISTA_COMPETICOES = ".selected-country-list>.leftMenu__item>a"
        CSS_BANDEIRA_PAIS = "span.breadcrumb__flag"

        documentoHtml = html_utils.converterTextoParaHtml(html)
        metadados = html_utils.obterMetadosHtml(html)
        nomePais = ""
        for dados in metadados:
            for key in dados:
                if key == "nome_pais":
                    nomePais = dados[key]
                if key == "url_pais":
                    urlPais = dados[key]
                if key == "hash_url_pais":
                    hashUrlPais = dados["hash_url_pais"]

        htmlBandeira = documentoHtml.select_one(CSS_BANDEIRA_PAIS)
        urlBandeiraPais = ";".join(htmlBandeira.attrs["class"])

        htmlListaCompeticoes = documentoHtml.select(CSS_LISTA_COMPETICOES)

        for htmlCompeticao in htmlListaCompeticoes:
            url = htmlCompeticao["href"]

            if url != "#":
                urlCompeticao = f"{navegador_web.URL_BASE}{url}"
                pais = Pais(
                    **{
                        "id": hashUrlPais,
                        "url": urlPais,
                        "nome": nomePais
                    })

                competicao = Competicao(
                    **{
                        "id": hash_utils.gerar_hash(urlCompeticao),
                        "nome": "",
                        "url": urlCompeticao,
                        "idPais": pais.id,
                        "nomePais": pais.nome
                    }
                )
                yield (pais,
                       competicao,
                       ItemExtracao(
                           **{
                               "uuid": auth_service.gerarIdentificadorUniversal(),
                               "url": urlCompeticao,
                               "hashUrl": hash_utils.gerar_hash(urlCompeticao),
                               "tipo": "EXT_HTML_COMPETICAO",
                               "extras": {"nome_pais": nomePais,
                                          "url_pais": urlPais,
                                          "hash_url_pais": hashUrlPais,
                                          "url_bandeira_pais": urlBandeiraPais}
                           }))

    except Exception as e:
        log.ERRO(f"Erro ao processar lista de competições do país [{urlCompeticao}]", e.args)
        return None


def processarHtmlEdicoesCompeticao(html: str):
    try:
        CSS_LISTA_EDICOES = "#tournament-page-archiv>div.archive__row"
        CSS_LINKS_EDICAO = ".archive__season>a"
        CSS_NOME_COMPETICAO = "div.heading__name"
        CSS_LOGO_COMPETICAO = "div.heading__logo"
        documentoHtml = html_utils.converterTextoParaHtml(html)

        metadados = html_utils.obterMetadosHtml(html)
        urlBandeiraPais = ""
        urlPais = ""
        hashUrlPais = ""

        for dados in metadados:
            for key in dados:
                if key == "url_pais":
                    urlPais = dados[key]
                if key == "nome_pais":
                    nomePais = dados[key]
                if key == "hash_url_pais":
                    hashUrlPais = dados["hash_url_pais"]
                if key == "url_competicao":
                    urlCompeticao = dados["url_competicao"]
                if key == "hash_url_competicao":
                    hashUrlCompeticao = dados["hash_url_competicao"]
                if key == "url_bandeira_pais":
                    urlBandeiraPais = dados["url_bandeira_pais"]

        nomeCompeticao = documentoHtml.select_one(CSS_NOME_COMPETICAO).text.strip()
        linksEdicoes = documentoHtml.select(CSS_LISTA_EDICOES)
        sequencial = 1

        for linha in linksEdicoes:
            links = linha.select("a")
            if len(links) == 0: continue

            temporada = links[0].text.split(" ")[-1]
            temporada = temporada.replace("/", "-").strip()
            inicioFimTemporada = edicao_competicao_core.obterInicioFimTemporada(temporada)

            urlEdicao = f"{urlCompeticao[:-1]}-{temporada}/"

            equipeVencedora = None

            if len(links) > 1:
                urlEquipeVencedora = f"{navegador_web.URL_BASE}{links[1]['href']}"
                equipeVencedora = {
                    "id": hash_utils.gerar_hash(urlEquipeVencedora),
                    "nome": links[1].text.strip(),
                    "url": urlEquipeVencedora
                }

            pais = Pais(**{
                "id": hashUrlPais,
                "url": urlPais,
                "nome": nomePais,
                "urlBandeira": urlBandeiraPais,
            })
            competicao = Competicao(
                **{
                    "id": hashUrlCompeticao,
                    "nome": nomeCompeticao,
                    "url": urlCompeticao,
                    "idPais": pais.id,
                    "nomePais": pais.nome
            })

            yield (pais, competicao, EdicaoCompeticao(
                **{
                    "id": hash_utils.gerar_hash(urlEdicao),
                    "idCompeticao": competicao.id,
                    "nomeCompeticao": competicao.nome,
                    "nomePais": pais.nome,
                    "url": urlEdicao,
                    "urlLogo": "",
                    "temporada": temporada,
                    "inicioTemporada": inicioFimTemporada[0],
                    "fimTemporada": inicioFimTemporada[1],
                    "equipeVencedora": equipeVencedora,
                    "emAndamento": equipeVencedora is None,
                    "sequencial": sequencial
                }))

            sequencial += 1

    except Exception as e:
        log.ERRO(f"Erro ao obter lista de edições da competição [{urlCompeticao}]", e.args)
        return None
