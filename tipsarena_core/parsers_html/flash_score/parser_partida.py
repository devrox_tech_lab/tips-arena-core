# -*- coding: utf-8 -*-
from datetime import datetime

from tipsarena_core.enums.enum_partida import STATUS as STATUS_PARTIDA
from tipsarena_core.extratores.flash_score import navegador_web
from tipsarena_core.models.partida import Partida
from tipsarena_core.services import log_service as log
from tipsarena_core.utils import (datetime_utils, hash_utils, html_utils,
                                  string_utils)


def processarHtmlPartidasEdicaoCompeticao(html: str):
    try:
        CSS_TABELA_PARTIDAS = "#live-table"
        CSS_LINHAS_PARTIDA = "div[id^=g_1_]"
        CSS_DATA_HORA_PARTIDA = ".event__time"
        CSS_NOME_MANDANTE = ".event__participant--home"
        CSS_ESCUDO_MANDANTE = ".event__logo--home"
        CSS_NOME_VISITANTE = ".event__participant--away"
        CSS_ESCUDO_VISITANTE = ".event__logo--away"
        CSS_PLACAR_FINAL = ".event__scores"
        CSS_PLACAR_PARCIAL = ".event__part"

        documentoHtml = html_utils.converterTextoParaHtml(html)
        metadados = documentoHtml.select_one("metadados").attrs

        tabelaPartidas = documentoHtml.select_one(CSS_TABELA_PARTIDAS)
        linhasHtml = tabelaPartidas.select(CSS_LINHAS_PARTIDA)

        sequencial = 1

        for item in linhasHtml:
            idPartida = item.attrs["id"].split("_")[2]
            urlPartida = f"{navegador_web.URL_BASE}/jogo/{idPartida}/"
            dataHoraPartida = "-" if item.select_one(CSS_DATA_HORA_PARTIDA) is None else item.select_one(
                CSS_DATA_HORA_PARTIDA).text

            nomeEquipeMandante = item.select_one(CSS_NOME_MANDANTE).text
            escudoEquipeMandante = item.select_one(CSS_ESCUDO_MANDANTE).attrs["src"] if "src" in item.select_one(
                CSS_ESCUDO_MANDANTE).attrs else "/res/image/data/IgO7K1ZA-Qmtm30D7.png"

            nomeEquipeVisitante = item.select_one(CSS_NOME_VISITANTE).text
            escudoEquipeVisitante = item.select_one(CSS_ESCUDO_VISITANTE).attrs["src"] if "src" in item.select_one(
                CSS_ESCUDO_VISITANTE).attrs else "/res/image/data/IgO7K1ZA-Qmtm30D7.png"

            placarFinal = item.select_one(CSS_PLACAR_FINAL).text

            placarParcial = "-" if item.find(CSS_PLACAR_PARCIAL) is None else item.select_one(CSS_PLACAR_PARCIAL).text

            yield {
                "idPartida": idPartida,
                "url": urlPartida,
                "dataHoraPartida": dataHoraPartida,
                "nomeEquipeMandante": string_utils.limparString(nomeEquipeMandante),
                "escudoEquipeMandante": f"{navegador_web.URL_BASE}{escudoEquipeMandante}",
                "nomeEquipeVisitante": string_utils.limparString(nomeEquipeVisitante),
                "escudoEquipeVisitante": f"{navegador_web.URL_BASE}{escudoEquipeVisitante}",
                "placarParcial": placarParcial,
                "placarFinal": placarFinal,
                "sequencial": sequencial,
                "metadadosEdicao": metadados}

            sequencial += 1

    except Exception as e:
        log.ERRO(f"Não foi possível processar lista de partidas da url {metadados['url_partidas']}.", e.args)
        return None


def processarHtmlListaDePartidas(html: str):
    try:
        CSS_TABELA_PARTIDAS = "#live-table"
        CSS_LINHAS_PARTIDA = "div[id^=g_1_]"
        CSS_DATA_HORA_PARTIDA = ".event__time"
        CSS_NOME_MANDANTE = ".event__participant--home"
        CSS_ESCUDO_MANDANTE = ".event__logo--home"
        CSS_NOME_VISITANTE = ".event__participant--away"
        CSS_ESCUDO_VISITANTE = ".event__logo--away"
        CSS_PLACAR_MANDANTE = ".event__score--home"
        CSS_PLACAR_VISITANTE = ".event__score--away"

        CSS_DADOS_CABECALHO = ".breadcrumb"
        CSS_LINKS_CABECALHO = "a.breadcrumb__link"

        CSS_DADOS_COMPETICAO = ".heading"
        CSS_NOME_COMPETICAO = ".heading__name"
        CSS_TEMPORADA = ".heading__info"
        CSS_LOGO_EDICAO = ".heading__logo"

        documentoHtml = html_utils.converterTextoParaHtml(html)
        metadados = html_utils.obterMetadosHtml(html)

        tabelaPartidas = documentoHtml.select_one(CSS_TABELA_PARTIDAS)
        linhasHtml = tabelaPartidas.select(CSS_LINHAS_PARTIDA)

        nomePais = ""
        urlPais = ""
        nomeCompeticao = ""
        temporada = ""
        logoEdicao = ""

        dadosCompeticao = documentoHtml.select_one(CSS_DADOS_COMPETICAO)
        dadosCabecalho = documentoHtml.select_one(CSS_DADOS_CABECALHO)

        if dadosCabecalho is not None:
            linksCabecalho = dadosCabecalho.select(CSS_LINKS_CABECALHO)
            nomePais = linksCabecalho[1].text
            urlPais = f"{navegador_web.URL_BASE}{linksCabecalho[1].attrs['href']}"

        if dadosCompeticao is not None:
            nomeCompeticao = dadosCompeticao.select_one(CSS_NOME_COMPETICAO).text
            temporada = dadosCompeticao.select_one(CSS_TEMPORADA).text
            logoEdicao = dadosCompeticao.select_one(CSS_LOGO_EDICAO).attrs["style"]

        sequencial = 1

        for item in linhasHtml:
            idPartida = item.attrs["id"].split("_")[2]
            urlPartida = f"{navegador_web.URL_BASE}/jogo/{idPartida}/"
            dataHoraPartida = "-" if item.select_one(CSS_DATA_HORA_PARTIDA) is None else item.select_one(
                CSS_DATA_HORA_PARTIDA).text

            nomeEquipeMandante = item.select_one(CSS_NOME_MANDANTE).text
            escudoEquipeMandante = item.select_one(CSS_ESCUDO_MANDANTE).attrs["src"] if "src" in item.select_one(
                CSS_ESCUDO_MANDANTE).attrs else "/res/image/data/IgO7K1ZA-Qmtm30D7.png"

            nomeEquipeVisitante = item.select_one(CSS_NOME_VISITANTE).text
            escudoEquipeVisitante = item.select_one(CSS_ESCUDO_VISITANTE).attrs["src"] if "src" in item.select_one(
                CSS_ESCUDO_VISITANTE).attrs else "/res/image/data/IgO7K1ZA-Qmtm30D7.png"

            yield {
                **{"id_partida": idPartida,
                   "url_partida": urlPartida,
                   "nome_pais": nomePais,
                   "url_pais": urlPais,
                   "nome_competicao": nomeCompeticao,
                   "temporada": temporada,
                   "url_logo_edicao": logoEdicao,
                   "nome_equipe_mandante": string_utils.limparString(nomeEquipeMandante),
                   "escudo_equipe_mandante": f"{navegador_web.URL_BASE}{escudoEquipeMandante}",
                   "nome_equipe_visitante": string_utils.limparString(nomeEquipeVisitante),
                   "escudo_equipe_visitante": f"{navegador_web.URL_BASE}{escudoEquipeVisitante}",
                   "sequencial": sequencial},
                **metadados
            }

            sequencial += 1

    except Exception as e:
        log.ERRO(f"Não foi possível processar lista de partidas do dia: {metadados}.", e.args)
        return None


def processarHtmlPartida(html: str):
    try:
        CSS_CABECALHO_PARTIDA = "[class*=tournamentHeaderDescription]"
        CSS_DADOS_MANDANTE = "[class^=duelParticipant__home]"
        CSS_DADOS_VISITANTE = "[class^=duelParticipant__away]"
        CSS_PLACAR_PARTIDA = "div[class^=detailScore__wrapper]"
        CSS_DATA_PARTIDA = "[class^=duelParticipant__startTime] div"
        CSS_STATUS_PARTIDA = "[class^=detailScore__status]"
        CSS_DETALHES_PARTIDA = "div[class^=infoBox__info]"
        CSS_LINKS_PARTIDA = "div.tabs div.tabs__group a.tabs__tab"

        htmlPartida = html_utils.converterTextoParaHtml(html)
        metadados = html_utils.obterMetadosHtml(html)

        htmlDadosCompeticao = htmlPartida.select_one(CSS_CABECALHO_PARTIDA)
        htmlDadosMandante = htmlPartida.select_one(CSS_DADOS_MANDANTE)
        htmlImageMandante = htmlDadosMandante.select_one("[class^=participant__image]")
        htmlNomeMandante = htmlDadosMandante.select_one("[class^=participant__participantNameWrapper] a")

        htmlDadosVisitante = htmlPartida.select_one(CSS_DADOS_VISITANTE)
        htmlImageVisitante = htmlDadosVisitante.select_one("[class^=participant__image]")
        htmlNomeVisitante = htmlDadosVisitante.select_one("[class^=participant__participantNameWrapper] a")

        htmlDataPartida = htmlPartida.select_one(CSS_DATA_PARTIDA)
        htmlStatusPartida = htmlPartida.select_one(CSS_STATUS_PARTIDA)
        htmlDetalhesPartida = htmlPartida.select_one(CSS_DETALHES_PARTIDA)
        htmlLinksPartida = htmlPartida.select(CSS_LINKS_PARTIDA)
        htmlPlacarPartida = htmlPartida.select_one(CSS_PLACAR_PARTIDA)
        htmlFaseCompeticao = htmlDadosCompeticao.select_one("[class^=tournamentHeader__country] a")

        urlPartida = ""

        for dados in metadados:
            for key in dados:
                if key == "url_partida":
                    urlPartida = dados[key]
                    pass

        status = htmlStatusPartida.text.strip().split("-")
        minutos = status[1] if len(status) > 1 else ""
        dataHora = htmlDataPartida.text
        detalhes = htmlDetalhesPartida.text.strip() if htmlDetalhesPartida is not None else ""
        placarFinal = htmlPlacarPartida.text.strip()

        urlCompeticao = f"{navegador_web.URL_BASE}{htmlFaseCompeticao.attrs['href']}"
        hashUrlCompeticao = hash_utils.gerar_hash(urlCompeticao)
        nomeCompeticao = htmlFaseCompeticao.text.split("-")[0]
        faseCompeticao = "-".join(htmlFaseCompeticao.text.split("-")[1::])

        nomeEquipeMandante = htmlNomeMandante.text
        urlEquipeMandante = f"{navegador_web.URL_BASE}{htmlNomeMandante.attrs['href']}"
        hashUrlEquipeMandante = hash_utils.gerar_hash(urlEquipeMandante)
        # idEquipeMandante = urlEquipeMandante.split("/")[-1]
        urlImageMandante = htmlImageMandante.attrs["src"]

        nomeEquipeVisitante = htmlNomeVisitante.text
        urlEquipeVisitante = f"{navegador_web.URL_BASE}{htmlNomeVisitante.attrs['href']}"
        hashUrlEquipeVisitante = hash_utils.gerar_hash(urlEquipeVisitante)
        # idEquipeVisitante = urlEquipeVisitante.split("/")[-1]
        urlImageVisitante = htmlImageVisitante.attrs["src"]

        competicao = {"url": urlCompeticao,
                      "nome": string_utils.limparString(nomeCompeticao),
                      "id": hashUrlCompeticao
                      }

        equipeMandante = {"id": hashUrlEquipeMandante,
                          "url": urlEquipeMandante,
                          "nome": string_utils.limparString(nomeEquipeMandante),
                          "urlEscudo": f"{navegador_web.URL_BASE}{urlImageMandante}"
                          }
        equipeVisitante = {"id": hashUrlEquipeVisitante,
                           "url": urlEquipeVisitante,
                           "nome": string_utils.limparString(nomeEquipeVisitante),
                           "urlEscudo": f"{navegador_web.URL_BASE}{urlImageVisitante}"
                           }

        return Partida(
            **{
                "id": hash_utils.gerar_hash(urlPartida),
                "urlPartida": urlPartida,
                "status": normalizarDescricaoStatus(status[0]),
                "dataHora": datetime.strptime(dataHora, "%d.%m.%Y %H:%M"),
                "dataHoraUtc": datetime_utils.converterHoraLocalToUtc(datetime.strptime(dataHora, "%d.%m.%Y %H:%M")),
                "minutos": string_utils.limparString(minutos),
                "detalhes": string_utils.limparString(detalhes),
                "faseCompeticao": string_utils.limparString(faseCompeticao),
                "placarFinal": string_utils.limparString(placarFinal),
                "competicao": competicao,
                "equipeMandante": equipeMandante,
                "equipeVisitante": equipeVisitante,
                "informacoesDisponiveis": obterInformacoesDisponiveis(htmlLinksPartida)
            })

    except Exception as e:
        log.ERRO("Não foi possível processar HMTL da partida.", e.args)
        return None


def obterInformacoesDisponiveis(htmlLinks):
    try:
        informacoesMap = {
            "resumo-de-jogo/resumo-de-jogo": "timeline",
            "resumo-de-jogo": "timeline",
            "resumo-de-jogo/estatisticas-de-jogo": "estatisticas",
            "resumo-de-jogo/equipes": "escalacao",
            "resumo-de-jogo/comentarios-ao-vivo": "comentarios",
            "comparacao-de-odds": "odds",
            "h2h": "h2h",
            "videos": "videos",
            "imagens-da-partida": "imagens",
            "noticias": "noticias",
            "classificacao": "classificacao",
            "draw": "tabela"
        }
        informacoes = {}
        for key in informacoesMap:
            informacoes[informacoesMap[key]] = False

        for link in htmlLinks:
            href = link.attrs["href"]
            info = href.split("#/")[1]
            informacoes[informacoesMap[info]] = True

        return informacoes
    except Exception as e:
        log.ERRO("Não foi possível verificar as informações disponíveis para a partida.", e.args)
        return informacoes


def normalizarDescricaoStatus(status: str):
    if status == "":
        statusPartida = STATUS_PARTIDA.AGENDADO.name

    elif status.find("1º tempo") != -1 or status.find("1st Half") != -1:
        statusPartida = STATUS_PARTIDA.PRIMEIRO_TEMPO.name

    elif status.find("2º tempo") != -1 or status.find("2nd Half") != -1:
        statusPartida = STATUS_PARTIDA.SEGUNDO_TEMPO.name

    elif status == "Adiado" or status == "Postponed":
        statusPartida = STATUS_PARTIDA.ADIADO.name

    elif status == "Após Pênaltis" or status == "After Penalties":
        statusPartida = STATUS_PARTIDA.FINALIZADO.name

    elif status == "Após Prorrogação" or status == "After Extra Time":
        statusPartida = STATUS_PARTIDA.FINALIZADO.name

    elif status == "Intervalo" or status == "Half Time" or status == "Break Time":
        statusPartida = STATUS_PARTIDA.INTERVALO.name

    elif status == "Atribuído" or status == "Awarded":
        statusPartida = STATUS_PARTIDA.RESULTADO_NAO_DISPONIVEL.name

    elif status == "Abandonado" or status == "Abandoned":
        statusPartida = STATUS_PARTIDA.ABANDONADO.name

    elif status == "Cancelado" or status == "Cancelled":
        statusPartida = STATUS_PARTIDA.CANCELADO.name

    elif status == "SRF - Só resultado final." or status == "FRO - Final result only.":
        statusPartida = STATUS_PARTIDA.RESULTADO_NAO_DISPONIVEL.name

    elif status == "SRF " or status == "FRO ":
        statusPartida = STATUS_PARTIDA.RESULTADO_NAO_DISPONIVEL.name

    elif status == "Encerrado" or status == "Finished":
        statusPartida = STATUS_PARTIDA.FINALIZADO.name

    elif status == "Walkover":
        statusPartida = STATUS_PARTIDA.W_O.name

    elif status.find("Walkover") != -1:
        statusPartida = STATUS_PARTIDA.W_O.name

    elif status == "Ao Vivo" or status == "Live":
        statusPartida = STATUS_PARTIDA.EM_ANDAMENTO.name

    elif status == "Extra Time":
        statusPartida = STATUS_PARTIDA.EM_ANDAMENTO.name

    elif status == "Penalties":
        statusPartida = "PENALTIES"

    else:
        log.ALERTA(f"Status '{status}' não mapeado.")
        statusPartida = status

    return statusPartida
