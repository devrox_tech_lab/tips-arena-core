# -*- coding: utf-8 -*-
""""
  Utilitário para gerenciamento logs da aplicação.
"""
import logging
import os
from datetime import datetime
from logging import ERROR, WARNING

AMBIENTE = os.getenv("TA_ENV")
LEVEL = WARNING if AMBIENTE == "DEV" else ERROR
logging.basicConfig(
    level=LEVEL
)


def OK(mensagem: str):
    print(f"\033[92mOK:     \033[00m | {datetime.now()} | {mensagem} |")


def ALERTA(mensagem: str):
    # logging.warning(mensagem)
    print(f"\033[93mALERTA: \033[00m | {datetime.now()} | {mensagem} |")


def INFO(mensagem: str):
    # logging.info(mensagem)
    print(f"\033[96mINFO:   \033[00m | {datetime.now()} | {mensagem} |")


def ERRO(mensagem: str, erros=""):
    # logging.error(f"{mensagem} | {erros}")
    print(f"\033[91mERRO:   \033[00m | {datetime.now()} | \033[91m{mensagem} | {erros} | ")
